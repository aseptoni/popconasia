<?php
//Template Name: Handler Milis
get_header();

$podsApi = pods_api();

$email = $_POST['email'];
$fullname = $_POST['fullname'];

if($email && $fullname){
	$personalInformation = array(
	'fullname'	=> $fullname,
	'email'	=> $email,
	'title'	=> $email,
	'post_status'	=> 'publish',
	);

	$podExist = pods( 'milis', $email );
	if ( $podExist->exists() ) {
		// the item exists
		$status=2;
	}
	else {
		// the item does not exist
		//insert the new one
		$podsMilis = $podsApi->save_pod_item(array(
			'pod'	=> 'milis',
			'data'	=> $personalInformation,
			'from'	=> 'frontend',
		  ));

		if($podsMilis){
			$status=1;
		}else{
			$status=0;
		}
	}


}else{
	$status=0;
}
?>
<br><br>
<!-- Title Section -->
<section class="judulatas">
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <h2 class="section-heading" style="color: white; text-transform: uppercase;">
        <?php echo the_title(); ?></h2>
        </div>
    </div>
</div>
</section>

<!-- Body Section -->
    <section>
        <div class="container">

             <div class="row">
              	<div class="col-lg-8">
                	 <div class="row text-center"><br><br>
                     <?php while($wp_query->have_posts()) : the_post();?>
                      <?php
					  if($status==1){
					  	echo the_content();
					  }elseif($status==2){
						  echo "<img src='".get_site_url()."/wp-content/uploads/2017/01/popo-300x300.png'/>";
					  	echo "<h3>Your email already on our list!</h3>";
					  }else{
						  echo "<img src='".get_site_url()."/wp-content/uploads/2017/01/popo-300x300.png'/>";
					  	echo "<h3>Seems like we encountered some error, please try again</h3>";
					  }
					  ?>
                     <?php endwhile; ?>
                     </div>
                </div>
                <div class="col-lg-4"><br><br>
                	<?php echo get_sidebar(); ?>
                </div>
             </div>

        </div>
    </section>
