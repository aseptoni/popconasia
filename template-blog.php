<?php
/*
Template Name: Blog
*/
get_header();


$cat=$_GET['cat'];


if ( get_query_var('paged') ) {

$paged = get_query_var('paged');

} elseif ( get_query_var('page') ) {

$paged = get_query_var('page');

} else {

   $paged = 1;

}


?>

    <br><br>

<?php
if ($cat=="press"){
    ?>
    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                <h2 class="section-heading" style="color: white; text-transform: uppercase; padding: 5px 0px;">PRESS RELEASE</h2>
                </div>

            </div>
        </div>
    </section>
<?php
}else{
?>

    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                <h2 class="section-heading" style="color: white; text-transform: uppercase; padding: 5px 0px;"><?php echo the_title(); ?></h2>
                </div>

            </div>
        </div>
    </section>

<style type="text/css">
            .linkna {
              color: white !important;

            }
            .linkna:hover{
              color: white !important;
              text-decoration: none;
            }
            </style>
    <!-- Slider Desktop -->
    <div class="visible-lg">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-top: -43px; height: 400px;">

        <?php $args = array(
                'post_type' => 'post',
                'tax_query' => array(
                 array(
                 'taxonomy' => 'category',
                 'field' => 'slug',
                 'terms' => 'slider-blog'
                 )
                 )
            );
            $wp_query = new WP_Query($args);
        ?>

      <!-- Indicators -->
    <ol class="carousel-indicators" style="left:90%;">

      <?php
       $hitung = 0;
      while($wp_query->have_posts()) :

            the_post();
       $active="";
       if ($hitung == 0) {
          $active="active";
       }
      ?>
      <li data-target="#carousel-example-generic" data-slide-to="<?=$hitung?>" class="<?=$active?>"></li>
        <?php
    $hitung = $hitung + 1;
    endwhile; ?>
    </ol>
        <br><br>
        <div class="carousel-inner" role="listbox">

            <?php
           // var_dump($wp_query);
            $hitung = 0;
            while($wp_query->have_posts()) :
            the_post();
            $hitung = $hitung + 1;
            //var_dump($wp_query);
            if ($hitung == 1) {

            ?>
            <div class="item active">
                <div class="row"><a href="<?php echo get_the_permalink(); ?>">
                    <div class="col-lg-8" style="padding: 0; background-image: url('<?php echo the_post_thumbnail_url(); ?>'); background-size: cover; background-position: center; height: 350px;">
                    </div></a>
                    <div class="col-lg-4" style="background: #283883; color: white; height: 350px; padding: 10px 30px 10px 15px;">
                        <a href="<?php echo get_the_permalink(); ?>" class="linkna"><h2 style="font-size: 2em;"><?php the_title(); ?></h2></a>
                        <div style="font-size: medium;"><?php the_excerpt(); ?></div><br>
                        <a href="<?php echo get_the_permalink(); ?>"><button class="btn" style="background-color: #E2595D; color: white;">Selengkapnya</button></a>
                    </div>
                </div>
            </div>
            <?php
                }
                else {
            ?>
                <div class="item">
                <div class="row"><a href="<?php echo get_the_permalink(); ?>">
                    <div class="col-lg-8" style="padding: 0; background-image: url('<?php echo the_post_thumbnail_url(); ?>'); background-size: cover; background-position: center; height: 350px;">
                    </div></a>
                    <div class="col-lg-4" style="background: #283883; color: white; height: 350px; padding: 10px 30px 10px 15px;">
                        <a href="<?php echo get_the_permalink(); ?>" class="linkna"><h2 style="font-size: 2em;"><?php the_title(); ?></h2></a>
                        <div style="font-size: medium;"><?php the_excerpt(); ?></div><br>
                        <a href="<?php echo get_the_permalink(); ?>"><button class="btn" style="background-color: #E2595D; color: white;">Selengkapnya</button></a>
                    </div>
                </div>
                </div>
            <?php }; endwhile; ?>
        </div>

      <!-- Controls -->
      <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a> -->
    </div>
    </div>


    <!-- Slider Mobile -->
    <div class="hidden-lg">
    <div id="carousel-example-generic-mobile" class="carousel slide" data-ride="carousel" style="margin-top: -65px;">
      <!-- Indicators -->

        <?php $args = array(
              'post_type' => 'post',
                'tax_query' => array(
                 array(
                 'taxonomy' => 'category',
                 'field' => 'slug',
                 'terms' => 'slider-blog'
                 )
                 )
            );
            $wp_query = new WP_Query($args);
            $hitungm = 0;
            while($wp_query->have_posts()) :
            the_post();
            $hitungm = $hitungm + 1;
            //var_dump($wp_query);
            if ($hitungm == 1) {

            ?>

     <!--  <ol class="carousel-indicators">

          <?php
           $hitung = 0;
          while($wp_query->have_posts()) :

            the_post();
             $active="";
             if ($hitung == 0) {
                  $active="active";
             }
            ?>
        <li data-target="#carousel-example-generic" data-slide-to="<?=$hitung?>" class="<?=$active?>"></li>
        <?php
        $hitung = $hitung + 1;
        endwhile; ?>
      </ol>  -->
        <br><br><br>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="col-lg-8" style="padding: 0; background-image: url('<?php echo the_post_thumbnail_url(); ?>'); background-size: cover; background-position: center; height: 250px; -webkit-filter: brightness(0.70); filter: brightness(0.70); ">
                    </div>
                    <div style="margin-top: -150px; z-index: 9999; margin-bottom: 50px; position: relative;">
                <a href="<?php echo get_the_permalink(); ?>">
                <center><h4 style="color: white;"><?php the_title(); ?></h4></center>
                </a>
                </div>
            </div>
            <?php
                }
                else {
            ?>
                <div class="item">
                <div class="col-lg-8" style="padding: 0; background-image: url('<?php echo the_post_thumbnail_url(); ?>'); background-size: cover; background-position: center; height: 250px; -webkit-filter: brightness(0.70); filter: brightness(0.70); ">
                    </div>
                    <div style="margin-top: -150px; z-index: 9999; margin-bottom: 50px; position: relative;">
                <a href="<?php echo get_the_permalink(); ?>">
                <center><h4 style="color: white; padding: 10px;"><?php the_title(); ?></h4></center>
                </a>
                </div>
                </div>
            <?php }; endwhile; ?>
      </div>

      <!-- Controls -->

      <a class="left carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    </div>


<?php
}
$perpage = 5;
if ($cat=="press") {
                $args = array( 'post_type' => 'post', 'posts_per_page' => $perpage, 'paged' => $paged,

    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'press-release'
        )
    ) );
}else{
    $args = array( 'post_type' => 'post', 'category__not_in' => 73, 'posts_per_page' => $perpage, 'paged' => $paged );
}
?>

    <!-- Body Section -->
    <section>
        <div class="container"><br>
            <div class="row">
                <div class="col-md-8">

            <?php

    $wp_query = new WP_Query($args);
    while ( have_posts() ) : the_post(); ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-12 hidden-lg" style="background-image: url('<?php echo the_post_thumbnail_url('full'); ?>'); background-size: cover; height: 150px; margin-left: 15px; background-position: center; width: 250px;">
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 visible-lg" style="background-image: url('<?php echo the_post_thumbnail_url('full'); ?>'); background-size: cover; height: 150px; margin-left: 15px; background-position: center; width: 150px;">
                        </div>
                        <div class="col-lg-9 col-md-9 col-xs-12">
                            <a style="font-size:25px; color:black;" href="<?php the_permalink(); ?>"><h4 class="judul-blog"><?php the_title(); ?></h4></a>
        <?php echo '<p>Posted on '.get_the_date('M d, Y').'</p>'; ?>
        <p><?php the_excerpt(); ?></p><a style="color:#CC1D22;" href="<?php the_permalink(); ?>">Baca Selengkapnya</a>

                        </div>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
     <center>
                <?php wp_pagenavi(); ?>
                </center>
                    </div>


            <div class="col-md-4 col-lg-4 col-sm-12 sidebar">
                  <?php
                    get_sidebar();
                  ?>
              </div>
                </div>
    <div class="col-md-8">

                </div>
            </div>
        </div>
    </section>



<?php
get_footer();
?>