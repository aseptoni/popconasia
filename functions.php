<?php
/**
 * theme_wp_setup
 * setup dasar untuk konfigurasi theme
 */
function theme_wp_setup()
{
	
	wp_enqueue_script("jquery");

    add_theme_support('automatic-feed-links');
    add_theme_support('html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ));
    // pengganti tag <title></title>
    add_theme_support('title-tag');

    // mengaktifkan post thumbnail
    add_theme_support('post-thumbnails');

    /* Register Menu */
    register_nav_menus(array(
        'primary_menu' => 'Primary Menu',
    ));

    if ( !defined( 'PODS_UPLOAD_REQUIRE_LOGIN' ) )
{
    define( 'PODS_UPLOAD_REQUIRE_LOGIN', false );
}


}

add_action('after_setup_theme', 'theme_wp_setup');

/**
 * Menambahkan Scipts Javascript dan CSS
 */

function theme_wp_scripts()
{
    /*
     * Adds JavaScript to pages with the comment form to support
     * sites with threaded comments (when in use).
     */
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
//    wp_enqueue_script("jquery");
    wp_enqueue_style( 'theme-media_queries', get_template_directory_uri() . '/media_queries.css' );

    wp_enqueue_script("js-jquery", get_template_directory_uri() . '/bootstrap/js/jquery.min.js');
    wp_enqueue_script("js-bootstrap", get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js');
    wp_enqueue_script("js-sticky-sidebar", get_template_directory_uri() . '/js/theia-sticky-sidebar.js');
    wp_enqueue_script("js-main", get_template_directory_uri() . '/js/main.js');


}

add_action('wp_enqueue_scripts', 'theme_wp_scripts');




/**
 * @param  $more from global variable
 * mengganti tanda '[...]' menjadi '....'
 */
function new_excerpt_more($more)
{
    return '....';
}

add_filter('excerpt_more', 'new_excerpt_more');

/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
function example_customizer( $wp_customize ) {

	$wp_customize->add_section(
        'popcon_section',
        array(
            'title' => 'Popcon Setting',
            'description' => 'Setting for several parts of theme.',
            'priority' => 35,
        )
    );

	$wp_customize->add_setting(
		'buy_label',
		array(
			'default' => 'Buy Ticket',
		)
	);

	$wp_customize->add_control(
		'buy_label',
		array(
			'label' => 'Buy Ticket label',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	$wp_customize->add_setting(
		'buy_url',
		array(
			'default' => 'https://popconasia.com',
		)
	);

	$wp_customize->add_control(
		'buy_url',
		array(
			'label' => 'Buy Ticket URL',
			'section' => 'popcon_section',
			'type' => 'url',
		)
	);

	/* ACTIVITIES */
	$wp_customize->add_setting(
		'judul_act',
		array(
			'default' => 'ACTIVITIES',
		)
	);

	$wp_customize->add_control(
		'judul_act',
		array(
			'label' => 'Judul Activities',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	$wp_customize->add_setting(
		'text_act',
		array(
			'default' => 'Cras malesuada finibus venenatis. Donec eget lectus non lacus molestie euixsod. Duis ultricies posuere pharetra.',
		)
	);

	$wp_customize->add_control(
		'text_act',
		array(
			'label' => 'Text Activities',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	$wp_customize->add_setting(
		'act_url',
		array(
			'default' => 'https://popconasia.com',
		)
	);

	$wp_customize->add_control(
		'act_url',
		array(
			'label' => 'Button Activities Url',
			'section' => 'popcon_section',
			'type' => 'url',
		)
	);

	$wp_customize->add_setting(
		'button_act',
		array(
			'default' => 'View More',
		)
	);

	$wp_customize->add_control(
		'button_act',
		array(
			'label' => 'Button Activities',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

/* Latest News */
	$wp_customize->add_setting(
		'judul_news',
		array(
			'default' => 'LATEST NEWS',
		)
	);

	$wp_customize->add_control(
		'judul_news',
		array(
			'label' => 'Judul News',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	$wp_customize->add_setting(
		'news_url',
		array(
			'default' => 'https://popconasia.com',
		)
	);

	$wp_customize->add_control(
		'news_url',
		array(
			'label' => 'Button News Url',
			'section' => 'popcon_section',
			'type' => 'url',
		)
	);

	$wp_customize->add_setting(
		'button_news',
		array(
			'default' => 'More News',
		)
	);

	$wp_customize->add_control(
		'button_news',
		array(
			'label' => 'Button News',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	/* WHO COMING */
	$wp_customize->add_setting(
		'judul_who',
		array(
			'default' => "WHO'S COMING",
		)
	);

	$wp_customize->add_control(
		'judul_who',
		array(
			'label' => 'Judul Who Coming',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	$wp_customize->add_setting(
		'text_who',
		array(
			'default' => 'Cras malesuada finibus venenatis. Donec eget lectus non lacus molestie euixsod. Duis ultricies posuere pharetra.',
		)
	);

	$wp_customize->add_control(
		'text_who',
		array(
			'label' => 'Text Who Coming',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	$wp_customize->add_setting(
		'who_url',
		array(
			'default' => 'https://popconasia.com',
		)
	);

	$wp_customize->add_control(
		'who_url',
		array(
			'label' => 'Button Who Coming Url',
			'section' => 'popcon_section',
			'type' => 'url',
		)
	);

	$wp_customize->add_setting(
		'button_who',
		array(
			'default' => 'View More',
		)
	);

	$wp_customize->add_control(
		'button_who',
		array(
			'label' => 'Button Who Coming',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	/* SPONSOR & PARTNER*/
	$wp_customize->add_setting(
		'judul_snp',
		array(
			'default' => 'SPONSOR & PARTNER',
		)
	);

	$wp_customize->add_control(
		'judul_snp',
		array(
			'label' => 'Judul SPONSOR & PARTNER',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	$wp_customize->add_setting(
		'judul_sponsor',
		array(
			'default' => 'GOLD SPONSOR',
		)
	);

	$wp_customize->add_control(
		'judul_sponsor',
		array(
			'label' => 'Judul Sponsor',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	$wp_customize->add_setting(
		'judul_partner',
		array(
			'default' => 'PARTNER',
		)
	);

	$wp_customize->add_control(
		'judul_partner',
		array(
			'label' => 'Judul Partner',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	/* Copyright */
	$wp_customize->add_setting(
		'copyright',
		array(
			'default' => 'Copyright.',
		)
	);

	$wp_customize->add_control(
		'copyright',
		array(
			'label' => 'Copyright text',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	/* LOGO */
	$wp_customize->add_setting( 'img-upload' );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'img-upload',
			array(
				'label' => 'Logo',
				'section' => 'popcon_section',
				'settings' => 'img-upload'
			)
		)
	);

	/* Day 1 */
	$wp_customize->add_setting(
		'day1',
		array(
			'default' => '',
		)
	);

	$wp_customize->add_control(
		'day1',
		array(
			'label' => 'Day 1 (day, date)',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	/* Day 2 */
	$wp_customize->add_setting(
		'day2',
		array(
			'default' => '',
		)
	);

	$wp_customize->add_control(
		'day2',
		array(
			'label' => 'Day 2 (day, date)',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	/* Day 3 */
	$wp_customize->add_setting(
		'day3',
		array(
			'default' => '',
		)
	);

	$wp_customize->add_control(
		'day3',
		array(
			'label' => 'Day 3 (day, date)',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);

	/* Footer desc */
	$wp_customize->add_setting(
		'footer_desc',
		array(
			'default' => '',
		)
	);

	$wp_customize->add_control(
		'footer_desc',
		array(
			'label' => 'Footer desc',
			'section' => 'popcon_section',
			'type' => 'text',
		)
	);


}
add_action( 'customize_register', 'example_customizer' );



// custom wp_nav_menu untuk nav-bar bootstrap
//require get_template_directory() . '/bootstrap-walker.php';
if ( function_exists('register_sidebars') )
register_sidebars(2);

# Menu
require_once('wp_bootstrap_navwalker.php');
function register_my_menu() {
 register_nav_menu('my-menu',__( 'My Menu' ));
}
add_action( 'init', 'register_my_menu' );

add_filter('show_admin_bar', '__return_false');


add_action( 'rest_api_init', 'slug_register_starship' );
function slug_register_starship() {

    register_rest_field( 'activities',
        'featured_media_url',
        array(
            'get_callback'    => 'slug_get_col1',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'company',
        'featured_media_url',
        array(
            'get_callback'    => 'slug_get_col1',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'schedule',
        'featured_media_url',
        array(
            'get_callback'    => 'slug_get_col1',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'exhibitor',
        'featured_media_url',
        array(
            'get_callback'    => 'slug_get_col1',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'speaker',
        'featured_media_url',
        array(
            'get_callback'    => 'slug_get_col1',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'slider',
        'featured_media_url',
        array(
            'get_callback'    => 'slug_get_col1',
            'update_callback' => null,
            'schema'          => null,
        )
    );
	
	/*register_rest_field( 'exhibitor',
        'exhibitor_type',
        array(
            'get_callback'    => 'get_exhibitor_type',
            'update_callback' => null,
            'schema'          => null,
        )
    );*/
	
}

function slug_get_col1( $object, $field_name, $request ) {
    //return "New";
	return get_the_post_thumbnail_url( $object[ 'id' ]);
}

function get_exhibitor_type($object, $field_name, $request){
	$terms = wp_get_post_terms( $object[ 'id' ],'exhibitor_categories',array("fields" => "all"));
	$terms = get_option( "exhibitor_categories_".$terms[0]->term_id );
	//$color = get_field('shortname', 'exhibitor_categories'.$terms['term_id']);
	//$categories = get_terms( 'exhibitor_categories', 'orderby=count&hide_empty=0' );
	//$pods = pods('exhibitor_categories',$terms['term_id']);
	return $terms;
}

//add custom fields on quick edit
function popcon_exhibitor_columns_head($defaults) {
    $defaults['booth_number'] = 'Booth Number';
    return $defaults;
}
function popcon_exhibitor_columns_content($column_name, $post_ID) {
    if ($column_name == 'booth_number') {
        echo get_post_meta($post_ID,"booth_number",true);
    }
}
add_filter('manage_exhibitor_posts_columns', 'popcon_exhibitor_columns_head', 10);
add_action('manage_exhibitor_posts_custom_column', 'popcon_exhibitor_columns_content', 10, 2);

add_action( 'quick_edit_custom_box', 'display_quickedit_exhibitor', 10, 2 );

function display_quickedit_exhibitor( $column_name, $post_type ) {
    static $printNonce = TRUE;
    if ( $printNonce ) {
        $printNonce = FALSE;
        wp_nonce_field( plugin_basename( __FILE__ ), 'exhibitor_edit_nonce' );
    }
    if($post_type =="exhibitor"){
    ?>
    <fieldset class="inline-edit-col-right inline-edit-book">
        <div class="inline-edit-col column-<?php echo $column_name; ?>">
            <label class="inline-edit-group">
                <?php
                switch ( $column_name ) {
                    case 'booth_number':
                        ?><span class="title">Booth Number</span><input name="<?=$column_name?>" /><?php
                        echo "<br><i>Fill this if you want to change existing exhibitor booth number</i>";
                        break;
                }
                ?>
            </label>
        </div>
    </fieldset>
    <?php
    }
}
add_action( 'save_post', 'save_exhibitor_meta' );

function save_exhibitor_meta( $post_id ) {
    /* in production code, $slug should be set only once in the plugin,
       preferably as a class property, rather than in each function that needs it.
     */
    $slug = 'exhibitor';
    if ( $slug !== $_POST['post_type'] ) {
        return;
    }
    if ( !current_user_can( 'edit_post', $post_id ) ) {
        return;
    }
    $_POST += array("{$slug}_edit_nonce" => '');
    if ( !wp_verify_nonce( $_POST["{$slug}_edit_nonce"],
        plugin_basename( __FILE__ ) ) )
    {
        return;
    }

    if ( isset( $_REQUEST['booth_number'] ) ) {
        update_post_meta( $post_id, 'booth_number', $_REQUEST['booth_number'] );
    }
}


/**
 * Custom admin login header logo
 */
function custom_login_logo() {
    echo '<style type="text/css">'.
             'h1 a { background-image:url(https://popconasia.com/wp-content/uploads/2017/03/cropped-2.png) !important; }'.
         '</style>';
}
add_action( 'login_head', 'custom_login_logo' );

?>