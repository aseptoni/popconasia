<?php
/*
Template Name: Press
*/
get_header();

if ( get_query_var('paged') ) {

$paged = get_query_var('paged');

} elseif ( get_query_var('page') ) {

$paged = get_query_var('page');

} else {

   $paged = 1;

}
?>

    <br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                <h2 class="section-heading" style="color: white; text-transform: uppercase; padding: 5px 0px;"><?php echo the_title(); ?></h2>
                </div>

            </div>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container"><br>
            <div class="row">
                <div class="col-md-8">

            <?php
                $perpage = 5;
                $args = array( 'post_type' => 'post', 'posts_per_page' => $perpage, 'paged' => $paged,

    'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => 'press-release'
        )
    ) );
    $wp_query = new WP_Query($args);
    while ( have_posts() ) : the_post(); ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-12 hidden-lg" style="background-image: url('<?php echo the_post_thumbnail_url('full'); ?>'); background-size: cover; height: 150px; margin-left: 15px; background-position: center; width: 250px;">
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 visible-lg" style="background-image: url('<?php echo the_post_thumbnail_url('full'); ?>'); background-size: cover; height: 150px; margin-left: 15px; background-position: center; width: 150px;">
                        </div>
                        <div class="col-lg-9 col-md-9 col-xs-12">
                            <a style="font-size:25px; color:black;" href="<?php the_permalink(); ?>"><h4 class="judul-blog"><?php the_title(); ?></h4></a>
        <?php echo '<p>Posted on '.get_the_date('M d, Y').'</p>'; ?>
        <p><?php the_excerpt(); ?></p><a style="color:#CC1D22;" href="<?php the_permalink(); ?>">Baca Selengkapnya</a>

                        </div>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
     <center>
                <?php wp_pagenavi(); ?>
                </center>
                    </div>


            <div class="col-md-4 col-lg-4 col-sm-12 sidebar">
                  <?php
                    get_sidebar();
                  ?>
              </div>
                </div>
    <div class="col-md-8">

                </div>
            </div>
        </div>
    </section>



<?php
get_footer();
?>