<?php
/*
Template Name: Spekaer Sidebar
*/
get_header();

?>

<br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                <h2 class="section-heading" style="color: white;"><?php echo get_the_title(); ?></h2>
                </div>
                <div class="col-md-4">
                <div class="text-right"><br><select class="form-control hide">
                    <option>Filter</option>
                    <option>Date</option>
                    <option>Name</option>
                </select>
                </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
            <?php
                $perpage = 15;
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array( 'post_type' => 'speaker', 'posts_per_page' => $perpage,
				'paged'=>$paged,
				 'orderby'   => array(
				 	'menu_order' => 'ASC',
				 	'title' => 'ASC',

					)
                 );

                $wp_query = new WP_Query($args);
              ?>

              <div class="row">
              	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
               		<div class="row text-center"><br><br>
            <?php
                while($wp_query->have_posts()) : the_post();
				$pod = pods( 'speaker', get_the_id() );
                    $jabatan = $pod->field('jabatan');
					$organisasi = $pod->field('organisasi');
                ?>
                <div class="col-md-4 col-lg-4 col-xs-6 konten"><a href="<?php the_permalink(); ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive img-speaker" alt=""></a>
                    <h4 class="service-heading" style="margin-bottom:0px;"><?php echo get_the_title(); ?></h4>
                    <p class="text-muted custom-pop-desc"><?php echo $jabatan; if($organisasi){ echo ", ".$organisasi; } ?></p>
                </div>
                <?php endwhile; ?>
                <div class="col-md-12 col-xs-12">
                	 <?php wp_pagenavi(); ?>
                </div>
            </div><!-- container -->
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 sidebar" style="margin-top:20px;"><br>
                	<?php echo get_sidebar(); ?>
                </div>
              </div>

        </div>
    </section>

<?php
get_footer();
?>