<?php
get_header();

$pod = pods( 'activities', get_the_id() );
$idna = get_the_id();
$tgl = $pod->field('tanggal');
$tanggal_selesai = $pod->field('tanggal_selesai');
$j_mulai = $pod->field('jam_mulai');
$j_akhir = $pod->field('jam_selesai');
$link = $pod->field('links');
$label_button = $pod->field('label_button');

$deskipsi_singkat = $pod->field('deskipsi_singkat');

?>


<br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <br><a href="https://dev.popconasia.com/our-activities/"><span class="label label-default" >Activities</span></a>
            <h2 class="section-heading" style="color: white; margin-top:0px; text-transform: uppercase;">
                <?php echo get_the_title(); ?>
            </h2>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
            <div class="row"><br>
              <div class="col-md-8 col-lg-8 col-sm-12">
              
                <!-- Detail Information -->  
                <div class="panel panel-default">
                <div class="panel-heading"><h5>DETAIL INFORMATION</h5></div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                  <img src="<?php echo the_post_thumbnail_url('full'); ?>" width="100%" >
                  <p>
                  <br>
                    <!-- 
                    <b><small>Date start:</small><br>
                    <?php 
                    $time = strtotime($tgl);
                    echo date('l, d M Y',$time); ?> </b><br/>
                        
                        
                    <b><small>Date end:</small><br>
                    <?php 
                    $time = strtotime($tanggal_selesai);
                    echo date('l, d M Y',$time); ?></b><br>
                    
                    <b><small>Time:</small><br>
                    <?php echo substr($j_mulai,0,-3); ?> - <?php echo substr($j_akhir,0,-3); ?> WIB </b>
                    <br><br> -->
                    <?php
                    if ( ! empty( $link ) ) {
                  ?>
                  <a href="<?php echo $link; ?>" class="btn btn-popcon"><?php echo $label_button; ?></a><br><br>
                  <?php  } ?>
                    
                  </p>
                  
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                    <p style="font-size:16px; font-weight:bold;"><?=$deskipsi_singkat;?></p>
                   <?php
				     while ( have_posts() ) : the_post();?>
                    <p><?php  echo str_replace("\r", "<br />", get_the_content('')); ?></p>
                   <?php 
				   		endwhile; //resetting the page loop
						wp_reset_query(); //resetting the page query
					?>
                    </div>
                  </div> <!-- row -->
                  
                  <br>
                </div>
                </div>
                <!-- end of Detail Information --> 		
                 

                <!-- Speaker -->
                <?php
                $speaker = $pod->field( 'speaker' );
                if ( ! empty( $speaker ) ) {
                ?>
                	<div class="panel panel-default">
                    <div class="panel-heading"><h5>SPEAKER</h5></div>
                    <div class="panel-body">
                      <div class="row text-center">
                       <?php
  foreach ( $speaker as $rel ) { 
    $id = $rel[ 'ID' ];
    ?>
                          <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6"><a href="<?php echo get_the_permalink($id); ?>">
                              <img src="<?php echo get_the_post_thumbnail_url($id); ?>" class="img-responsive img-circle" alt="" ></a>
                              <a href="<?php echo get_the_permalink($id); ?>"><h5 class="service-heading"><?php echo get_the_title($id); ?></h5></a>
                              
                          </div>
<?php
    }
?>            
                      </div>
                    </div>                    
                  </div>
				<?php
                }  
                ?>
                <!-- end speaker -->

            <!-- Exhibitor / Company -->
            <?php 
            $exhibitor = $pod->field( 'exhibitor' );
            $company = $pod->field( 'company' );
            if (!empty( $exhibitor ) || !empty( $company )) {
            ?>
            <div class="panel panel-default">
            <div class="panel-heading"><h5>EXHIBITOR / COMPANY</h5></div>
            <div class="panel-body">
            <div class="row text-center">
            <?php
            
            //Looping exhibitor
            if(!empty( $exhibitor )){
            foreach ( $exhibitor as $rel ) { 
            $id = $rel[ 'ID' ];
            ?>
            <div class="col-md-3 col-lg-3 col-xs-6"><a href="<?php echo get_the_permalink($id); ?>">
            <?php
                    if (has_post_thumbnail()) {
                   ?>
            <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="">
            <?php }else{ ?>
            <img src="https://dev.popconasia.com/wp-content/uploads/2017/01/Untitled-2-01.png" class="img-responsive" alt="">
            <?php } ?>
            </a>
            <a href="<?php echo get_the_permalink($id); ?>"><h5 class="service-heading"><?php echo get_the_title($id); ?></h5></a>
            </div>   
            <?php } //end Looping exhibitor 
            }
            ?> 
            
            
            <?php
            //Looping company
            if(!empty( $company )){
            foreach ( $company as $rel ) { 
            $id = $rel[ 'ID' ];
            ?>
            <div class="col-md-3 col-lg-3 col-xs-6">
            <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="">
            <a href="<?php echo get_the_permalink($id); ?>"><h5 class="service-heading"><?php echo get_the_title($id); ?></h5></a>
            </div>   
            <?php  } //end Looping company 
            }
            ?>   
                            
            </div>
            </div>
            </div>
            <?php } ?>
            <!-- End Exhibitor / Company -->


            <!-- More activities -->
            <?php
            $perpage = 4;
            $the_query = query_posts(
            array(
            'post__not_in' => array($idna),
            'post_type'=>'activities',
            'posts_per_page'=>$perpage,
            'paged'=>$paged,
            'orderby'=>'rand'
            )
            );
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;          
            // $wp_query = new WP_Query($args);
            ?>
            <div class="panel panel-default">
            <div class="panel-heading"><h5>MORE ACTIVITIES</h5></div>
            <div class="panel-body"> 
            <div class="row text-center">
            <?php
            while($wp_query->have_posts()) : the_post();
            ?>   
            <div class="col-md-3 col-lg-3 col-xs-6"><a href="<?php the_permalink(); ?>">
            <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
            <a href="<?php the_permalink(); ?>"><h5 class="service-heading"><?php echo get_the_title(); ?></h5></a>
            
            </div>
            <?php endwhile; ?>
            </div>
            </div> 
            </div>
			<!-- End activities -->


              </div>

              <div class="col-md-4 col-lg-4 col-sm-12">
                <?php
                    echo get_sidebar();
                  ?>
              </div>
              
            </div>
        </div>
    </section>

<?php get_footer(); ?>