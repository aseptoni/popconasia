<?php
  //Template Name: Proses Form
  get_header();
  ?>

    <br><br>

    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                 <h2 class="section-heading" style="color: white; padding: 5px 0px;">
			<small style="color:#FFFFFF; font-size:14px;">Modcon 2017</small><br/><?php echo the_title(); ?>
            </h2>
                </div>
            </div>
        </div>
    </section>
<br><br>
<!-- Container -->
<div class="container">
  <!-- Blog Section Content -->
  <div class="row">
    <!-- Blog Single Page -->
    <div class="col-lg-8">
      <div class="" id="">

      	<?php

			$podsApi = pods_api();

			//Populate personal information from html form
			$fullname = $_POST['fullname'];
			$email = $_POST['email'];
			$mobile_number = $_POST['mobile_number'];
			$home_number = $_POST['home_number'];
			$address = $_POST['address'];
			$date_of_birth = $_POST['date_of_birth'];
			$nationality = $_POST['nationality'];
			$university = $_POST['university'];
			$major = $_POST['major'];
			$passport_number = $_POST['passport_number'];
			$passport_expiry_date = $_POST['passport_expiry_date'];
			$instagram_account = $_POST['instagram_account'];
			$twitter_account = $_POST['twitter_account'];
			$facebook_account = $_POST['facebook_account'];
			$youtube_account = $_POST['youtube_account'];
			$short_bio = $_POST['short_bio'];
			$artwork_title = $_POST['artwork_title'];
			$artwork_desc = $_POST['artwork_desc'];
			// $have_you_upload = $_POST['have_you_upload'];
			if(isset($_POST['have_you_upload'])){
				$have_you_upload = 'Yes';
			}else{
				$have_you_upload = 'No';
			}

			$self_photo = $_POST['self-photo'];
			$alt_photo = $_POST['alt-photo'];
			$artwork_file = $_POST['artwork_file'];
			$alt_artwork = $_POST['alt-artwork'];



			$photo="";
			if($self_photo){
				foreach($self_photo as $row){
					$photo=$row['id'];
				}
			}
			$self_photo = $photo;

			$artwork="";
			if($artwork_file){
				foreach($artwork_file as $row){
					$artwork=$row['id'];
				}
			}
			$artwork_file = $artwork;

			//Populate personal information
			$personalInformation = array(
				'title'	=> $fullname,
				'post_content' => 'Application form of '.$fullname,
				'full_name'	=> $fullname,
				'email'	=> $email,
				'mobile_number'	=> $mobile_number,
				'home_number' => $home_number,
				'address'	=> $address,
				'date_of_birth'	=> $date_of_birth,
				'nationality'	=> $nationality,
				'university'	=> $university,
				'major'	=> $major,
				'passport_number'	=> $passport_number,
				'passport_expiry_date'	=> $passport_expiry_date,
				'instagram' => $instagram_account,
				'twitter'	=> $twitter_account,
				'facebook'	=> $facebook_account,
				'youtube'	=> $youtube_account,
				'short_biography'	=> $short_bio,
				'self-photo'	=> $self_photo,
				'alternatif_photo'	=> $alt_photo,
				'artwork_title'	=> $artwork_title,
				'artwork_description'	=> $artwork_desc,
				'artwork_file'	=> $artwork_file,
				'alternatif_artwork' => $alt_artwork,
				'have_upload_ig'	=> $have_you_upload,
				'post_status'	=> 'publish'
			);

			// $ok = true;
			// foreach ($personalInformation as $key => $value) {
			// 	if ($value = "") {
			// 			$ok = false;
			// 		}
			// }
			// if(($artwork_file) && ($self_photo)){
				if( isset($_POST['fullname']) && $_POST['fullname']){
					$podsSubmissionModcon = $podsApi->save_pod_item(array(
						'pod'	=> 'submission_modcon',
						'data'	=> $personalInformation
			  			));
				}
			// }
		?>


        <?php  the_post(); the_content(); ?>
        </div>
      <?php //comments_template( '', true );?>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-12 sidebar" style="margin-top:20px;">
                	<?php echo get_sidebar(); ?>
                </div>
  </div>
</div>
<?php get_footer();?>
<?php if($_GET['submited']!="yes"): ?>
<script>
	window.location='https://popconasia.com/modcon/submission-process?submited=yes';
</script>
<?php endif; ?>