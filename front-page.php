<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>" />

  <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php bloginfo( 'name' ); ?></title>

    <?php

  $footer_desc = get_theme_mod("footer_desc");

  ?>


    <link href="<?php bloginfo( 'template_directory' ); ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
     <script src="<?php bloginfo( 'template_directory' ); ?>/assets/bootstrap/js/jquery.js"></script>
      <script src="<?php bloginfo( 'template_directory' ); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){

            $('.carousel').carousel();

            // $('.ninja-forms-field').addClass("form-control");

          $('#search').on('show.bs.modal', function () {

             $('#s').focus()

          });

          var url = $("#aftermovieVideo").attr('src');

          /* Assign empty url value to the iframe src attribute when
          modal hide, which stop the video playing */
          $("#aftermovie-highlight").on('hide.bs.modal', function(){
              $("#aftermovieVideo").attr('src', '');
          });

          /* Assign the initially stored url back to the iframe src
          attribute when modal is displayed again */
          $("#aftermovie-highlight").on('show.bs.modal', function(){
              $("#aftermovieVideo").attr('src', url + '?autoplay=1');
          });
        });




    </script>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_directory' ); ?>/assets/style.css?v=1.36">

      <link type="text/css" rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/slick/slick.css"  media="screen,projection"/>

      <link type="text/css" rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/slick/slick-theme.css"  media="screen,projection"/>

      <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/bootstrap/css/magnific-popup.css">
      <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/bootstrap/css/animate.css">

<!--<script type="text/javascript" src="<?=get_template_directory_uri()?>/slick/slick.min.js"></script>-->

<style type="text/css">

body{

  font-size: 15px !important;
}

.modal-backdrop.in {

  z-index: 500;

}

#bs-example-navbar-collapse-1{

  text-align: center;

}

</style>

<!-- <script id="socital-script" src="https://plugin.socital.com/static/v1/socital.js" data-socital-user-id="5873564256b15e4bbab23564"></script> -->

</head>

<?php

$logo = get_theme_mod("img-upload");

 ?>

<body>

<div id="fb-root"></div>

<script>(function(d, s, id) {

  var js, fjs = d.getElementsByTagName(s)[0];

  if (d.getElementById(id)) return;

  js = d.createElement(s); js.id = id;

  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=199086303495070";

  fjs.parentNode.insertBefore(js, fjs);

}(document, 'script', 'facebook-jssdk'));</script>

    <!-- Navigation -->

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" style="background-color: white; height:52px;">

        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header page-scroll">

                <a class="right hidden-lg" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="position: absolute; right: 5%; top: 30%;">

                    <img class="" src="https://popconasia.com/wp-content/uploads/2017/01/icon-burger.png" style="width: 25px; ">

                </a>

                <a class="navbar-brand hidden-xs hidden-sm" href="https://popconasia.com/" style="padding:10px;"><img src="<?php echo $logo; ?>" style="height:35px"></a>

                <a class="navbar-brand hidden-md hidden-lg" href="https://popconasia.com/"><img src="<?php echo $logo; ?>" style="height:25px"></a>

            </div>





            <!-- Collect the nav links, forms, and other content for toggling -->

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background-color: white; height:0px !important;">

       <?php

                        $buy_label = get_theme_mod("buy_label");

                        $buy_url = get_theme_mod("buy_url");

                     ?>

<?php

 $menu_args = array(

  'menu'    => 'my-menu',

  'theme_location' => 'my-menu',

  'depth'    => 4,

  'container'   => false,

  'menu_class'   => 'nav navbar-nav navbar-right',

  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',

  'walker'    => new wp_bootstrap_navwalker()

 );

 wp_nav_menu($menu_args);

?>

            </div>

            <!-- /.navbar-collapse -->

        </div>

        <!-- /.container-fluid -->



<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">

  <div class="modal-dialog modal-md" role="document">

    <div class="modal-content">

      <div class="modal-body">

        <form method="get" id="searchform" action="<?php bloginfo('url'); ?>">

        <div class="input-group">

        <input type="text" class="form-control" name="s" id="s" placeholder="Search for..." autofocus>

        <input type="hidden" name="search-type" value="normal" />

        <span class="input-group-btn">

          <input class="btn btn-default" type="submit" value="Go!!" name="submit">

          </span>

      </div><!-- /input-group -->

      </form>

      </div>

    </div>

  </div>

</div>

    </nav>



<?php


// ACTIVITIES SECTION
$judul_act = get_theme_mod("judul_act");
$text_act = get_theme_mod("text_act");
$button_act = get_theme_mod("button_act");
$url_act = get_theme_mod("act_url");

// NEWS SECTION
$judul_news = get_theme_mod("judul_news");
$button_news = get_theme_mod("button_news");
$url_news = get_theme_mod("news_url");

// WHO'S COMING SECTION
$judul_who = get_theme_mod("judul_who");
$text_who = get_theme_mod("text_who");
$button_who = get_theme_mod("button_who");
$url_who = get_theme_mod("who_url");

// SPONSOR & PARTNER
$judul_snp = get_theme_mod("judul_snp");
$judul_sponsor = get_theme_mod("judul_sponsor");
$judul_partner = get_theme_mod("judul_partner");

?>
    <!-- Slider Desktop -->
    <div class="visible-lg">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-top: 5px;">

        <?php $args = array(
        'post_type' => 'slider',
        'tax_query' => array(
          array(
          'taxonomy' => 'slider_type',
          'field' => 'slug',
          'terms' => 'desktop'
          )
          )
      );
      $wp_query = new WP_Query($args);
    ?>

      <!-- Indicators -->
    <ol class="carousel-indicators">

      <?php
       $hitung = 0;
      while($wp_query->have_posts()) :

            the_post();
       $active="";
       if ($hitung == 0) {
          $active="active";
       }
      ?>
      <li data-target="#carousel-example-generic" data-slide-to="<?=$hitung?>" class="<?=$active?>"></li>
        <?php
    $hitung = $hitung + 1;
    endwhile; ?>
    </ol>
        <br><br>
        <div class="carousel-inner" role="listbox">

            <?php
           // var_dump($wp_query);
            $hitung = 0;
            while($wp_query->have_posts()) :
            the_post();
            $hitung = $hitung + 1;
            //var_dump($wp_query);
            if ($hitung == 1) {

            ?>
            <div class="item active">
                <img src="<?php echo the_post_thumbnail_url(); ?>" >
            </div>
            <?php
                }
                else {
            ?>
                <div class="item">
                    <img src="<?php echo the_post_thumbnail_url(); ?>" >
                </div>
            <?php }; endwhile; ?>
        </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
    </div>

    <!-- Slider Mobile -->
    <div class="hidden-lg">
    <div id="carousel-example-generic-mobile" class="carousel slide" data-ride="carousel" style="margin-top: -10px;">
      <!-- Indicators -->
      <!-- <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic-mobile" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic-mobile" data-slide-to="1"></li>
      </ol> -->
        <br><br><br>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <?php $args = array(
              'post_type' => 'slider',
              'tax_query' => array(
              array(
                                    'taxonomy' => 'slider_type',
                                    'field' => 'slug',
                                    'terms' => 'mobile'
                            )
              )
            );
            $wp_query = new WP_Query($args);
            $hitungm = 0;
            while($wp_query->have_posts()) :
            the_post();
            $hitungm = $hitungm + 1;
            //var_dump($wp_query);
            if ($hitungm == 1) {

            ?>
            <div class="item active">
                <img src="<?php echo the_post_thumbnail_url(); ?>" >
            </div>
            <?php
                }
                else {
            ?>
                <div class="item">
                    <img src="<?php echo the_post_thumbnail_url(); ?>" >
                </div>
            <?php }; endwhile; ?>
      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    </div>

    <!-- Activities Section
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading"><?= $judul_act; ?></h2>
                    <br>
                </div>
            </div>
            <?php /*
                $perpage = 3;
                $args = array( 'post_type' => 'activities', 'posts_per_page' => $perpage,
                 'tax_query' => array(
                      array(
                        'taxonomy' => 'activities_categories',
                        'field'    => 'slug',
                        'terms'    => 'show-on-home'
                      )));
                $wp_query = new WP_Query($args);
              ?>
            <div class="row text-center">
            <?php
                while($wp_query->have_posts()) : the_post();
                 $pod = pods( 'activities', get_the_id() );
                    $desk = $pod->field('deskipsi_singkat');
          $picture_no_gif = $pod->field('picture_no_gif');
          ?>
          <pre class="hide">
          <?php
                    var_dump($picture_no_gif);
                ?>
                </pre>
                <div class="col-md-4 col-lg-4 col-sm-12 konten"><a href="<?php the_permalink(); ?>">
                    <div class="visible-lg">
                  <img src="<?php echo $picture_no_gif['guid']; ?>" class="img-nogif img-responsive" alt="">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-gif img-responsive hide" alt="">
                    </div>
                    <div class="hidden-lg">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="" style="width: 100%;">
                    </div>
                    </a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;">
                    <h4 class="service-heading" style="display:none;"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted" style="margin-top:5px;"><?php
                        if (strlen($desk) > 70)
                        $desk = substr($desk, 0, 70) . '...';
                    echo $desk; ?></p>
                </div>
                 <?php endwhile; ?>
            </div>
        </div>
            <div class="col-lg-12 col-md-12 col-xs-12 text-center">
            <!-- <p><? /*$text_act;*/ ?></p>
            <a href="<?php echo $url_act; ?>"><button class="btn btn-popcon"><?= $button_act ?></button></a><br><br><br>
        </div>
    </section> -->

    <!-- Who Coming Section -->
<!--
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading"><?= $judul_who ?></h2>
                    <br>
                </div>
            </div>
            <?php
               /* $perpage = 4;
                $args = array( 'post_type' => 'speaker', 'posts_per_page' => $perpage,
                 'tax_query' => array(
              array(
                'taxonomy' => 'speaker_categories',
                'field'    => 'slug',
                'terms'    => 'show-on-home'
              )));
                $wp_query = new WP_Query($args);
              ?>
            <div class="row text-center">
            <?php
                while($wp_query->have_posts()) : the_post();
          $pod = pods( 'speaker', get_the_id() );
                   $jabatan = $pod->field('jabatan');
          $organisasi = $pod->field('organisasi');
                ?>
                <div class="col-md-3 col-lg-3 col-xs-6 konten"><a href="<?php the_permalink(); ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;"><h4 class="service-heading"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted"><?php echo $jabatan;

          if($organisasi){
           echo ", ".$organisasi;
          }

          ?></p>
                </div>
                 <?php endwhile; ?>
            </div>
        </div>
            <div class="col-lg-12 col-md-12 col-xs-12 text-center">
            <h5><?= $text_who; ?></h5>
            <br>
            <a href="<?php echo $url_who; */ ?>"><button class="btn btn-popcon"><?= $button_who; ?></button></a><br><br><br>
        </div>
    </section>
-->
    <!-- News -->
     <section id="late-news ">
        <div class="container-fluid late-news ">
          <?php
                $perpage = 4;
                $args = array( 'post_type' => 'post', 'posts_per_page' => $perpage,
                 'tax_query' => array(
                      array(
                        'taxonomy' => 'post_on_home',
                        'field'    => 'slug',
                        'terms'    => 'yes'
                      )));
                $wp_query = new WP_Query($args);
              ?>
          <ul class="late-news-list">

              <?php
                while($wp_query->have_posts()) : the_post();
                $desk = get_the_excerpt();
                ?>
            <li class="col-md-3 col-lg-3 col-sm-6 col-xs-12 past-late-news">
              <a href="<?php the_permalink(); ?>">
                <div class="news">
                  <div class="news-photo">

                    <img class=" img-responsive" src="<?php echo the_post_thumbnail_url('full'); ?>">
                  </div>
                  <div class="news-text">
                     <h5>POPARAZZI</h5>
                    <h4 style="color: white"><?php echo get_the_title(); ?></h4>

                  </div>

                </div>
                </a>
              </li>
                <?php endwhile; ?>
            </ul>
          </div>
       </section>

    <!-- Subscribe Section -->
    <section class="subscribe">
      <div class="row-centered subscribe-row">
          <h4><?php echo get_option('landing_milis_heading'); ?></h4>
          <h5><?php echo get_option('landing_milis_subheading'); ?></h5>
          <form class="form-inline" target="_blank" id="formSubscribe" action="<?=get_site_url()?>/milis-process" method="POST">

            <input type="text" name="fullname" required class="form-control subscribe-input-name" placeholder="Name">
          <div class="input-group">

            <input type="text" name="email" required class="form-control subscribe-input-email" placeholder="Email">

            <div class="input-group-btn">
            <input type="submit" name="subscribe" class="btn btn-subscribe" value="<?php echo get_option('landing_milis_button_label'); ?>">
            </div>

            </div>
          </form>
      </div>
    </section>

    <!-- After Moive Section -->
    <section class="aftermovie">
        <div class="container-fluid">
          <div class="row">
             <?php
                                    $aftermovie_image = get_option( 'landing_after_movie_image');
                                    $aftermovie_image = wp_get_attachment_url( $aftermovie_image[0] );

                                   ?>


            <div class="col-md-6 col-lg-6 col-xs-12 aftermovie-col-left image-gallery" style="background: url(&quot;<?=$aftermovie_image;?>&quot;);background-repeat: no-repeat;
          background-position: center center;
          background-size: cover;"> <div class="item">
                    <a class="aftermovie-still-play magnific-youtube" href="<?php echo get_option('landing_after_movie_video'); ?>"  ></a>
                    </div>
            </div>

            <div class="col-md-6 col-lg-6 col-xs-12 aftermovie-col-right" >
              <div class="aftermovie aftermovie-col-text" >
              <h5><?php echo get_option('landing_after_movie_subheading'); ?></h5>
              <h1><?php echo get_option('landing_after_movie_heading');?></h1>
              </div>
              <div class="col-lg-12 col-md-12 col-xs-12 image-gallery"><div class="item">
                    <a class="magnific-youtube" href="<?php echo get_option('landing_after_movie_video'); ?>"  ><button class="btn btn-more"><?php echo get_option('landing_after_movie_button_label');?></button></a></div>
             </div>
            </div>
          </div>
        </div>

        <!-- MODAL -->
        <div class="modal fade" id="aftermovie-highlight" tabindex="-1" role="dialog" aria-labelledby="modal-video-label">
            <div class="modal-dialog aftermovie-modal-dialog" role="video">
                <div class="aftermovie-modal-content">
                    <div class="aftermovie-modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="aftermovie-modal-body">
                        <div class="modal-video">

                                <iframe id="aftermovieVideo" class="embed-responsive-item" width="700" height="393" src="<?php echo get_option('landing_after_movie_video'); ?>" frameborder="0" allowfullscreen></iframe>

                        </div>
                    </div>
                </div>
            </div>
        </div>



    </section>
    <!-- Theme of the year Section -->
    <section class="themeoftheyear">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12 themeoftheyear-col-left" >
                <div class="themeoftheyear themeoftheyear-col-text">
                <h5><?php echo get_option('landing_new_heading') ?></h5>
                <h1><?php echo get_option('landing_new_subheading') ?></h1>
                <p><?php echo get_option('landing_new_description') ?></p>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
            <a href="<?php echo get_option('landing_new_link') ?>"><button class="btn btn-more"><?php echo get_option('landing_new_label_button') ?></button></a>
             </div>
            </div>

             <?php
                                    $new_image = get_option( 'landing_new_image');
                                    $new_image = wp_get_attachment_url( $new_image[0] );

                                   ?>
            <div class="col-md-6 col-lg-6 col-xs-12 themeoftheyear-col-right " style="background: url(&quot;<?=$new_image;?>&quot;); background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;">


            </div>
          </div>
        </div>
    </section>


    <!-- Sponsor Partner Section -->
    <center>
    <section class="sponsor-partner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading"><?= $judul_snp; ?></h2>
                    <br>
                </div>
            </div>
            <?php
                        $categories = get_terms( array(
              'taxonomy'=>'categories_company',
              'orderby'=>'slug'
            ));
                        // var_dump($categories);
                        foreach($categories as $rowCat):
                          ?>
                      <div class="col-lg-12 col-md-12 col-xs-12 text-center section-margin">
                    <h3 class="section-heading" ><?=$rowCat->name;?></h3>
                    <br>
            <?php
            $slug=$rowCat->slug;
                $args = array( 'post_type' => 'Company', 'orderby'  => 'title',
                      'order'    => 'ASC',
                    'posts_per_page' => -1,
                'tax_query' => array(
                      array(
                        'taxonomy' => 'categories_company',
                        'field'    => 'slug',
                        'terms'    =>  $slug,

                      )));
                $wp_query = new WP_Query($args);
              ?>
            <div class="row row-centered text-center">
                <?php
                while($wp_query->have_posts()) : the_post();
                $pod = pods( 'Company', get_the_id() );

                $link_company = $pod->field( 'link_company' );
        if($slug=="asponsor"){
          $collg="col-lg-2";
        }else{
          $collg="col-lg-2";
        }
                ?>
                <div class="col-centered col-md-2 col-xs-6 <?=$collg;?> konten"><a target="_blank" href="<?php echo $link_company; ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive img-company" alt=""></a>
                </div>
                <?php endwhile; ?>
            </div>
                </div>
            <br>
                     <?php endforeach; ?>


        </div>
    </section>
</center>

<!-- Gallery -->
  <section class="gallery">
    <div class="container-fluid gallery-container" >
      <div class="mosaic">
        <?php
                                    $featured_gallery = get_option( 'landing_featured_gallery');
                                    $i=0;
                                    foreach ($featured_gallery as $images) {
                                      # code...
                                       $images = wp_get_attachment_url( $featured_gallery[$i] );
                                       $i++



                                   ?>
      <div class="col-md-6 mosaic-image" style="background-image: url(&quot;<?=$images?>&quot;);"></div>
      <?php } ?>

      </div>
      <!--
       <div class="col-lg-12 col-md-12 col-xs-12 text-center row-btn-gallerymore">
          <a href="#"><button class="btn btn-more">VIEW MORE</button></a>
             </div> -->
      </div>
</section>


  <!-- popstar Section -->
  <section id="past-popstars" class="past-popstars">
  <div class="container-fluid popstars" >
    <div class="row popstars">
      <div class="col-xs-12 col-md-12 col-lg-12">
      <h2 class="section-heading text-center"><?php echo get_option('landing_popstar_heading'); ?></h2>
      </div>
    </div>
    <div class="col-xs-12" style="margin-bottom: 20px !important;"></div>






    <ul class="popstars-list">
    <?php
              $perpage = 6;
              $order = "az";
               if($order=="newest"){
                    $orderArg = array(
                        'orderby'   => 'date',
                        'order'         => 'DESC'
                    );
                }elseif($order=="oldest"){
                    $orderArg = array(
                        'orderby'   => 'date',
                        'order'         => 'ASC'
                    );
                }elseif($order=="az"){
                    $orderArg = array(
                        'orderby'   => 'title',
                        'order'         => 'ASC'
                    );
                }elseif($order=="za"){
                    $orderArg = array(
                        'orderby'   => 'title',
                        'order'         => 'DESC'
                    );
                }else{
                    $orderArg = array(
                        'orderby'   => 'date',
                        'order'         => 'DESC'
                    );
                }
                $args = array( 'post_type' => 'speaker', 'posts_per_page' => $perpage,
                  'paged' => $paged,
                 'tax_query' => array(
              array(
                'taxonomy' => 'speaker_categories',
                'field'    => 'slug',
                'terms'    => 'show-on-home'
              )));
                $args = array_merge($args,$orderArg);
                $wp_query = new WP_Query($args);

                  while($wp_query->have_posts()) : the_post();
          $pod = pods( 'speaker', get_the_id() );
                   $jabatan = $pod->field('jabatan');
          $organisasi = $pod->field('organisasi');
              ?>


                  <li class="col-xs-6 col-sm-4 col-md-2 col-lg-2 popstar">
          <a href="<?php the_permalink();?>">
        <div class="popstar">
          <div class="popstar-photo">
            <img class="popstar-img img-responsive" src="<?php echo get_the_post_thumbnail_url() ?>">
          </div>
          <div class="popstar-text">
            <h4><?php echo get_the_title();?></h4>
            <p>
                              <span class="popstar-position"> <?php echo $jabatan;   if($organisasi){
           echo ", ".$organisasi;
          }

          ?></span>




                          </p>
          </div>

                  </div>
          </a>
      </li>

            <?php endwhile; ?>

      </ul>


      <div class="col-lg-12 col-md-12 col-xs-12 text-center row-btn-popstarmore">
            <a href="<?php echo get_option('landing_popstar_link'); ?>"><button class="btn btn-more"><?php echo get_option('landing_popstar_label_button'); ?></button></a>
             </div>
</div>
</section>
  <!-- End popstar Section -->

<?php
$copy = get_theme_mod("copyright");
$footer_desc = get_theme_mod("footer_desc");
 ?>

    <!-- footer Section -->
    <section class="bawah">
        <div class="container">
            <div class="row"><br><br><br><br>
                <div class="col-md-4 col-lg-4 col-sm-6" style="padding-bottom: 20px;">
                    <h4 class="service-heading">Popcon Asia</h4>
                    <p class="text-muted"><?=$footer_desc;?></p>
                        <div style="margin-top:40px;">

                        	<?php
								$fbLogo = get_option('popcon_general_facebook_logo');
								$fbLogo = wp_get_attachment_url($fbLogo[0]);
								$fbLink = get_option('popcon_general_facebook_link');

								$twLogo = get_option('popcon_general_twitter_logo');
								$twLogo = wp_get_attachment_url($twLogo[0]);
								$twLink = get_option('popcon_general_twitter_link');

								$igLogo = get_option('popcon_general_instagram_logo');
								$igLogo = wp_get_attachment_url($igLogo[0]);
								$igLink = get_option('popcon_general_instagram_link');

								$ytLogo = get_option('popcon_general_youtube_logo');
								$ytLogo = wp_get_attachment_url($ytLogo[0]);
								$ytLink = get_option('popcon_general_youtube_link');

							?>

                        	<a href="<?=$fbLink?>" target="_blank">
                            	<img src="<?=$fbLogo?>"
                                width="25"/>
                            </a>
                            <a href="<?=$twLink?>" target="_blank">
                            	<img src="<?=$twLogo?>"
                                width="25"/>
                            </a>
                            <a href="<?=$igLink?>" target="_blank">
                            	<img src="<?=$igLogo?>"
                                width="25"/>
                            </a>
                            <a href="<?=$ytLink?>" target="_blank">
                            	<img src="<?=$ytLogo?>"
                                width="25"/>
                            </a>
                        </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-3" style="padding-bottom: 20px;">
                    <h4 class="service-heading">Exhibitor Info</h4>
                    <p class="text-muted">
                    <?php
                         $menu = wp_get_nav_menu_object("footer1");
                         $menu_items = wp_get_nav_menu_items($menu->term_id);

                         foreach($menu_items as $row){
                            ?>
                            <a href='<?php echo $row->url; ?>' style='color:white;'>
                            <?php
                            echo "<h5>".$row->title."</h5></a>";
                         }
                          ?>
                    </p>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-3" style="padding-bottom: 20px;">
                    <h4 class="service-heading">Visitor Info</h4>
                    <p class="text-muted">
                    <?php
                         $menu = wp_get_nav_menu_object("footer2");
                         $menu_items = wp_get_nav_menu_items($menu->term_id);

                         foreach($menu_items as $row){
                            ?>
                            <a href='<?php echo $row->url; ?>' style='color:white;'>
                            <?php
                            echo "<h5>".$row->title."</h5></a>";
                         }
                          ?>
                    </p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6">
                   <div class="fb-page" data-href="https://www.facebook.com/PopconAsia/" data-tabs="timeline" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/PopconAsia/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/PopconAsia/">Popcon ASIA</a></blockquote></div><br>
                </div>
            </div>
        </div><br>
        <div class="foot">
            <div class="text-center" style="background-color: #E2595D; padding: 10px; color: white; margin-top: 5px;">
                <?= $copy; ?>
            </div>
        </div>
    </section>

  <!-- jQuery -->


    <!-- Bootstrap Core JavaScript -->

      <!--<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
      <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
      <script src="<?=get_template_directory_uri()?>/assets/bootstrap/js/masonry.pkgd.js"></script>
      <script src="<?=get_template_directory_uri()?>/assets/bootstrap/js/masonry.js"></script>
       <script src="<?=get_template_directory_uri()?>/assets/bootstrap/js/jquery.magnific-popup.min.js"></script>
      <script src="<?=get_template_directory_uri()?>/assets/bootstrap/js/wow.min.js"></script>
<script src="<?=get_template_directory_uri()?>/assets/bootstrap/js/scripts.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri()?>/slick/slick.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('.autoplay').slick({
  slidesToShow: 4,
  slidesToScroll: 4
});
    });

  function getPathFromUrl(url) {
  return url.split("?")[0];
}


  //Exhibitor
  $('.filter-select').change(function(e) {
    var url       = getPathFromUrl(window.location.href);
    var thisval     = $(this).val();
    var orderVal  = $('.order-select').val();
    window.location = url+'?filter='+thisval+'&order='+orderVal;

    });

  $('.order-select').change(function(e) {
    var thisval     = $(this).val();
    var filterVal = $('.filter-select').val();
    var url       = getPathFromUrl(window.location.href);
    window.location = url+'?filter='+filterVal+'&order='+thisval;
  });

  //Schedule
  $('.filter-select-schedule').change(function(e) {
    var url       = getPathFromUrl(window.location.href);
    var thisval     = $(this).val();
    window.location = url+'?filter='+thisval;

    });

  //Gif - nogif
  $('.img-nogif').hover(
    function() {
        $(this).addClass("hide");
    $(this).next('.img-gif').removeClass("hide");
    }
  );

  $( ".img-gif" ).mouseout(function() {
      $(this).addClass("hide");
      $(this).prev('.img-nogif').removeClass("hide");
  });



  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89931599-1', 'auto');
  ga('send', 'pageview');

</script>
<?php
 wp_footer();
?>
</body>
</html>