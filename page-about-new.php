<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>" />

  <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php bloginfo( 'name' ); ?></title>

    <?php

  $footer_desc = get_theme_mod("footer_desc");

  ?>

    

    <link href="<?php bloginfo( 'template_directory' ); ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/bootstrap/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php bloginfo( 'template_directory' ); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){

            $('.carousel').carousel();

            // $('.ninja-forms-field').addClass("form-control");

          $('#search').on('show.bs.modal', function () {

             $('#s').focus()

          });
          
          var url = $("#aboutsubdescVideo").attr('src');
    
          /* Assign empty url value to the iframe src attribute when
          modal hide, which stop the video playing */
          $("#aboutsubdesc-highlight").on('hide.bs.modal', function(){
              $("#aboutsubdescVideo").attr('src', '');
          });
          
          /* Assign the initially stored url back to the iframe src
          attribute when modal is displayed again */
          $("#aboutsubdesc-highlight").on('show.bs.modal', function(){
              $("#aboutsubdescVideo").attr('src', url);
          });
        });




    </script>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_directory' ); ?>/assets/style.css">
    

      <link type="text/css" rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/slick/slick.css"  media="screen,projection"/>

      <link type="text/css" rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/slick/slick-theme.css"  media="screen,projection"/>
      <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/bootstrap/css/masonry.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/bootstrap/css/magnific-popup.css">
      <link rel="stylesheet" href="<?=get_template_directory_uri()?>/assets/bootstrap/css/animate.css">

<!--<script type="text/javascript" src="wp-content/themes/Popcon%20Asia/slick/slick.min.js"></script>-->

<style type="text/css">

body{

  font-size: 15px !important;
}

.modal-backdrop.in {

  z-index: 500;

}

#bs-example-navbar-collapse-1{

  text-align: center;

}

</style>

<!-- <script id="socital-script" src="https://plugin.socital.com/static/v1/socital.js" data-socital-user-id="5873564256b15e4bbab23564"></script> -->

</head>


<body> 

<div id="fb-root"></div>

<script>(function(d, s, id) {

  var js, fjs = d.getElementsByTagName(s)[0];

  if (d.getElementById(id)) return;

  js = d.createElement(s); js.id = id;

  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=199086303495070";

  fjs.parentNode.insertBefore(js, fjs);

}(document, 'script', 'facebook-jssdk'));</script>

    <!-- Navigation -->

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" style="background-color: white; height:52px;">

        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header page-scroll">

                <a class="right hidden-lg" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="position: absolute; right: 5%; top: 30%;">

                    <img class="" src="wp-content/uploads/2017/01/icon-burger.png" style="width: 25px; ">

                </a>

                <a class="navbar-brand hidden-xs hidden-sm" href="" style="padding:10px;"><img src="wp-content/uploads/2017/01/logo-8-e1483447614630.png" style="height:35px"></a>

                <a class="navbar-brand hidden-md hidden-lg" href=""><img src="wp-content/uploads/2017/01/logo-8-e1483447614630.png" style="height:25px"></a>

            </div>

      

            

            <!-- Collect the nav links, forms, and other content for toggling -->

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background-color: white; height:0px !important;">

                 

<ul id="menu-menu-1" class="nav navbar-nav navbar-right"><li id="menu-item-85" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-85 active"><a title="HOME" href="http://popconasia.com">HOME</a></li>
<li id="menu-item-81" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81"><a title="ABOUT" href="http://popconasia.com/about-popcon-asia">ABOUT</a></li>
<li id="menu-item-80" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80"><a title="WHO’S COMING" href="http://popconasia.com/whos-coming">WHO’S COMING</a></li>
<li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a title="EXHIBITOR" href="http://popconasia.com/exhibitor">EXHIBITOR</a></li>
<li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a title="ACTIVITIES" href="http://popconasia.com/activities">ACTIVITIES</a></li>
<li id="menu-item-83" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-83 dropdown"><a title="MORE" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">MORE <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
  <li id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a title="SCHEDULE" href="http://popconasia.com/schedule">SCHEDULE</a></li>
  <li id="menu-item-97" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-97"><a title="BLOG" href="http://popconasia.com/blog">BLOG</a></li>
  <li id="menu-item-399" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-399"><a title="CONTACT" href="http://popconasia.com/contact">CONTACT</a></li>
  <li id="menu-item-445" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-445"><a title="PRESS RELEASE" href="http://popconasia.com/blog?cat=press">PRESS RELEASE</a></li>
</ul>
</li>
<li id="menu-item-91" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-91"><a title="&lt;!-- &lt;a style=&quot;margin:-5px;  padding: 0px 20px; &quot;&gt;&lt;img src=&quot;http://dev.popconasia.com/wp-content/uploads/2017/01/buy-ticket-button-blue-01-e1483529164631.png&quot; width=&quot;110px&quot; style=&quot;margin-top:-20px;&quot;&gt;&lt;/a&gt; --&gt;" href="http://#"><!-- <a style="margin:-5px;  padding: 0px 20px; "><img src="http://dev.popconasia.com/wp-content/uploads/2017/01/buy-ticket-button-blue-01-e1483529164631.png" width="110px" style="margin-top:-20px;"></a> --></a></li>
<li id="menu-item-92" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-92"><a title="&lt;a style=&quot;margin-top: -28px&quot; data-toggle=&quot;modal&quot; data-target=&quot;.bs-example-modal-sm&quot;&gt;&lt;span class=&quot;glyphicon glyphicon-search&quot; aria-hidden=&quot;true&quot;&gt;&lt;/span&gt;&lt;/a&gt;" href="http://#"><a style="margin-top: -28px" data-toggle="modal" data-target=".bs-example-modal-sm"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a></a></li>
</ul>
            </div>

            <!-- /.navbar-collapse -->

        </div>

        <!-- /.container-fluid -->

  

<div id="search" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">

  <div class="modal-dialog modal-md" role="document">

    <div class="modal-content">

      <div class="modal-body">

        <form method="get" id="searchform" action="http://popconasia.com">

        <div class="input-group">

        <input type="text" class="form-control" name="s" id="s" placeholder="Search for..." autofocus>

        <input type="hidden" name="search-type" value="normal" /> 

        <span class="input-group-btn">

          <input class="btn btn-default" type="submit" value="Go!!" name="submit">

          </span>

      </div><!-- /input-group -->

      </form>

      </div>

    </div>

  </div>

</div>

    </nav>

   
  
  

     <!-- Sponsor Partner Section -->
    <center>
    <section class="aboutheader">
        <div class="container">
           
             <br><br><br><br>
             <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h1 class="section-heading aboutborder-bottom1">ABOUT

                    </h1>
                    <br>
                </div>
            </div>
             <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <img src="<?=get_template_directory_uri()?>/assets/img/about/about-header-img.jpg" >
                </div>
            </div>
             <div class="row">
              
                <div class="col-lg-9 col-md-9 col-xs-12 aboutheader-text col-centered">
                    <p class=" text-center">Pop Con Asia is the premier pop culture event by Pop Con Inc, dedicated to creating awareness of, and appreciation for creative and popular art forms.</p>
                </div>
               
                </div>
            </div>   
                
        </div>
 </section>
</center>
       
         



  
    <!-- Theme of the year Section -->
    <section class="aboutdesc">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-7 col-lg-7 col-xs-12 aboutdesc-col-left wow fadeInUp">
                <div class="aboutdesc aboutdesc-col-text">
               <p >It is an expertly curated convention and festival where creators from all aspects of the popular arts, curators and fans converge to celebrate the latest content from comics, games, TV, film, toys, music and technology.</p>
<p>In its current Jakarta-based incarnation, Pop Con Asia strives to be a must-attend event on the pop culture convention schedule.</p>
                <hr class="about-hr1">
                </div>

              
            </div>
            <div class="col-md-5 col-lg-5 col-xs-12 aboutdesc-col-right wow fadeIn" data-wow-delay="300ms">
              

            </div>
          </div>
        </div>
    </section>

    <!-- Subdesc Section -->
    <section class="aboutsubdesc">
        <div class="container-fluid ">
          <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-12 aboutsubdesc-col-left wow fadeIn" data-wow-delay="300ms"
data-wow-delay="300ms">
           
            </div>
            <div class="col-md-8 col-lg-8 col-xs-12 aboutsubdesc-col-right wow fadeInUp">
              <div class="aboutsubdesc aboutsubdesc-col-text" >
              
              <h2>WHAT IS POP CULTURE ?
              <hr class="about-hr1"></h2>
              
              <p>The definition of Pop Culture (or Popular Culture) in general is the entirety of ideas, perspective, attitudes, images, and other phenomena that are within the mainstream of a given culture. Heavily influenced by mass media, this collection of ideas permeates the everyday lives of the society.</p>
<p>Pop Culture Event is a planned public or social occasion that celebrates the historic and ongoing contribution of popular contents to art and culture.</p>
              </div>
              
            </div>
          </div>
        </div>
        </section>
        

         <!-- Key Facts Section -->
    <center>
    <section class="aboutkeyfacts">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h1 class="section-heading aboutborder-bottom1">KEY FACTS</h1>
                    <br>
                    <br>
                </div>
           
            </div>
            <div class="row wow fadeInUp">
                <div class="col-lg-10 col-md-10 col-xs-12 col-md-offset-1 col-lg-offset-1 text-center">
                  
                       
                      
                    <div class="text-center-centered col-md-3 col-xs-12 col-lg-3 konten aboutkeyfacts-text"><span>89.210</span><hr class="about-hr2"/>
                    <p>Visitors</p>
                    </div>
                    <div class="text-center-centered col-md-3 col-xs-12 col-lg-3 konten aboutkeyfacts-text"><span>471</span><hr class="about-hr2"/>
                    <p >Exhibitors</p>
                    </div>
                    
                    <div class="text-center-centered col-md-3 col-xs-12 col-lg-3 konten aboutkeyfacts-text"><span>7</span><hr class="about-hr2"/>
                    <p>Countries</p>
                    </div>
                    
                     <div class="text-center-centered col-md-3 col-xs-12 col-lg-3 konten aboutkeyfacts-text"><span >6</span><hr class="about-hr2"/>
                    <p >Big Events</p>
                    </div>
                    
                    
                    
               
                    
                
                                
                </div>
             </div>
            
                
        </div>
 </section>
</center>
       

 




  <!-- Milestones Section -->

  <section class="aboutmilestones-section">
    <div class="container">
      <div class="row">
       
          <div class="col-lg-12 col-md-12 col-xs-12 text-center aboutmilestones-header">
              <h1 class="section-heading aboutborder-bottom1">
                  MILESTONES
              </h1>
              <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-2 col-md-offset-2">
              <p>
                The event has grown in all aspects over the years: <br>more attendees, more exhibitors, more programs, and more fun !
              </p>
              </div>
          </div>
     
      </div>


     
      <div class="row milestones">

            <a href="#" class="col-md-4 col-lg-4 col-sm-4 col-xs-12 milestone-list">
              <div class="milestone">
              
                <div class="milestone-photo">
                  <img class="" src="<?=get_template_directory_uri()?>/assets/img/about/popcon2012.jpg"> 
                </div>
              
               <div class="milestone-text">
               
                 <span>2012</span><i class="glyphicon glyphicon-new-window"></i>
                 <p>Very first movements</p>
            
               </div>
              
              </div>
           </a>

  
            <a href="#" class="col-md-4 col-lg-4 col-sm-4 col-xs-12 milestone-list">
              <div class="milestone">
              
                <div class="milestone-photo">
                  <img class="" src="<?=get_template_directory_uri()?>/assets/img/about/popcon2013.jpg"> 
                </div>
             
               <div class="milestone-text">
              
                 <span>2013</span><i class="glyphicon glyphicon-new-window"></i>
                 <p>Be seen, be heard, be famous</p>
              
               </div>
                
              </div>
             </a>

            <a href="#" class="col-md-4 col-lg-4 col-sm-4 col-xs-12 milestone-list">
              <div class="milestone">
              
                <div class="milestone-photo">
                  <img class="" src="<?=get_template_directory_uri()?>/assets/img/about/popcon2014.jpg"> 
                </div>
        
               <div class="milestone-text">
              
                 <span>2014</span><i class="glyphicon glyphicon-new-window"></i>
                 <p>Be pop, be you !</p>
               
               </div>
              
              </div>
              </a>
</div>
<div class="row milestones">
           <a href="#" target="" class="col-md-4 col-lg-4 col-sm-4 col-xs-12 milestone-list">
              <div class="milestone">
              
                <div class="milestone-photo">
                  <img class="" src="<?=get_template_directory_uri()?>/assets/img/about/popcon2015.jpg"> 
                </div>
               <div class="milestone-text">
              
                 <span>2015</span><i class="glyphicon glyphicon-new-window"></i>
                 <p>Conquer your creative space</p>
                
               </div>
              
              </div>
             </a>

            <a href="#" class="col-md-4 col-lg-4 col-sm-4 col-xs-12 milestone-list">
              <div class="milestone">
              
                <div class="milestone-photo">
                  <img class="" src="<?=get_template_directory_uri()?>/assets/img/about/popcon2016.jpg"> 
                </div>
             
               <div class="milestone-text">
               
                 <span>2016</span><i class="glyphicon glyphicon-new-window"></i>
                 <p>Pop Revolution !</p>
                
               </div>
               
              </div>
            </a>

            <a href="#" class="col-md-4 col-lg-4 col-sm-4 col-xs-12 milestone-list">
              <div class="milestone">
              
                <div class="milestone-photo">
                  <img class="" src="<?=get_template_directory_uri()?>/assets/img/about/popcon2017.jpg"> 
                </div>
              
               <div class="milestone-text milestone-this-year">
               
                 <span>2017</span><i class="glyphicon glyphicon-new-window"></i>
                 <p>Pop Parade</p>
                
               </div>
             
              </div>
              </a>



  
      </div>
      </div>
      </section>

  <!-- End aboutmilestone Section -->

  <!-- Objectives Section -->

  <section class="aboutobjectives-section">
    <div class="container">
      <div class="row ">
       
          <div class="col-lg-12 col-md-12 col-xs-12 text-center aboutobjectives-header">
              <h1 class="section-heading aboutborder-bottom1">
                  OBJECTIVES
              </h1>
              <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-2 col-md-offset-2">
              <p>
               We are creative platform
              </p>
              </div>
          </div>
     
      </div>
      <div class="row centered">
          <div class="col-lg-6 col-md-6 col-xs-12 objective-list">
              <div  class="col-md-4 col-lg-4 col-xs-12 text-center ">
                <img class="objective-photo " src="<?=get_template_directory_uri()?>/assets/img/about/objective-img-market.png">
                   
                
              </div>
              <div class="col-md-8 col-lg-8 col-xs-12 objective-text"> 
                  <span>Market</span>
                  <p>For creative talents to show and market their work</p>
              </div>
          </div>
         <div class="col-lg-6 col-md-6 col-xs-12 objective-list">
             <div  class="col-md-4 col-lg-4 col-xs-12 text-center ">
                 <img class="objective-photo " src="<?=get_template_directory_uri()?>/assets/img/about/objective-img-network.png">
                   
              </div>
               <div class="col-md-8 col-lg-8 col-xs-12 objective-text">
                  <span>Network</span>
                  <p>For creative talents to meet, share ideas, and expanding their network</p>
              </div>
          </div>
          <div class="col-lg-6 col-md-6 col-xs-12 objective-list">
              <div  class="col-md-4 col-lg-4 col-xs-12 text-center ">
                <img class="objective-photo " src="<?=get_template_directory_uri()?>/assets/img/about/objective-img-appreciation.png">
                   
              </div>
              <div class="col-md-8 col-lg-8 col-xs-12 objective-text">
                  <span>Appreciation</span>
                  <p>For professional creative workers and general public</p>
              </div>
          </div>
          <div class="col-lg-6 col-md-6 col-xs-12 objective-list">
           <div  class="col-md-4 col-lg-4 col-xs-12 text-center ">
                 <img class="objective-photo " src="<?=get_template_directory_uri()?>/assets/img/about/objective-img-learn.png">
                   
              </div>
               <div class="col-md-8 col-lg-8 col-xs-12 objective-text">
                  <span>Learn</span>
                  <p>Of appreciation for professional creative workers and general public</p>
              </div>
          </div>
      </div>      
    </div>
  </section>

  <!-- End Objectives Section -->

  <!-- Participants Section -->
  <section class="aboutparticipants-section">
    <div class="container">
      <div class="row ">
       
          <div class="col-lg-12 col-md-12 col-xs-12 text-center aboutparticipants-header">
              <h1 class="section-heading aboutborder-bottom2">
                  PARTICIPANTS
              </h1>
             
          </div>
     
      </div>
      <div class="row">
          <div class="col-lg-3 col-md-3 col-xs-12 participant-list">
            <div class="col-lg-4 col-md-4">
              <img class="participant-img" src="<?=get_template_directory_uri()?>/assets/img/about/participants-img-book.png">
            </div>
            <div class="col-lg-8 col-md-8 participant-text">
              <span>Comic Artist & Publisher</span>
            </div>   
          </div>

          <div class="col-lg-3 col-md-3 col-xs-12 participant-list">
             <div class="col-lg-4 col-md-4">
              <img class="participant-img" src="<?=get_template_directory_uri()?>/assets/img/about/participants-img-brush.png">
            </div>
            <div class="col-lg-8 col-md-8 participant-text">
              <span>Illustrators</span>
            </div>   
          </div>

          <div class="col-lg-3 col-md-3 col-xs-12 participant-list">
            <div class="col-lg-4 col-md-4">
              <img class="participant-img" src="<?=get_template_directory_uri()?>/assets/img/about/participants-img-gamecontroller.png">
            </div>
            <div class="col-lg-8 col-md-8 participant-text">
              <span>Game Studios, Designers</span>
            </div>   
          </div>
            
          <div class="col-lg-3 col-md-3 col-xs-12 participant-list">
            <div class="col-lg-4 col-md-4">
              <img class="participant-img" src="<?=get_template_directory_uri()?>/assets/img/about/participants-img-movie.png">
            </div>
            <div class="col-lg-8 col-md-8 participant-text">
              <span>Filmmakers, Producers, Studios</span>
            </div>   
          </div>
      </div>
    </div>
  </section>

    <!-- footer Section -->
    <section class="bawah">
        <div class="container">
            <div class="row"><br><br><br><br>
                <div class="col-md-4 col-lg-4 col-sm-6" style="padding-bottom: 20px;">
                    <h4 class="service-heading">Popcon Asia</h4>
                    <p class="text-muted">“Popular Culture Convention” Asia is the biggest festival in Asia to celebrate creative contents from the emerging local and global creator to the growing market of teens and young-adults, and family.</p>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-3" style="padding-bottom: 20px;">
                    <h4 class="service-heading">Exhibitor Info</h4>
                    <p class="text-muted">
                     
                            <a href='#' style='color:white;'>
                            <h5>All Exhibitor</h5></a> 
                            <a href='#' style='color:white;'>
                            <h5>Exhibitor Info</h5></a>                    </p>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-3" style="padding-bottom: 20px;">
                    <h4 class="service-heading">Visitor Info</h4>
                    <p class="text-muted">
                     
                            <a href='http://popconasia.com/activities' style='color:white;'>
                            <h5>Activities</h5></a> 
                            <a href='#' style='color:white;'>
                            <h5>Venue Detail</h5></a> 
                            <a href='#' style='color:white;'>
                            <h5>Direction</h5></a> 
                            <a href='http://popconasia.com/blog' style='color:white;'>
                            <h5>aboutmilestone</h5></a>                    </p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6">
                   <div class="fb-page" data-href="https://www.facebook.com/PopconAsia/" data-tabs="timeline" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/PopconAsia/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/PopconAsia/">Popcon ASIA</a></blockquote></div><br>
                </div>
            </div>
        </div><br>
        <div class="foot">
            <div class="text-center" style="background-color: #E2595D; padding: 10px; color: white; margin-top: 5px;">
                Copyright 2017 ©  Popcon Asia            </div>
        </div>
    </section>

  
   
<script src="<?=get_template_directory_uri()?>/assets/bootstrap/js/masonry.pkgd.js"></script>
            <script src="<?=get_template_directory_uri()?>/assets/bootstrap/js/masonry.js"></script>
 <script src="assets/bootstrap/js/jquery.magnific-popup.min.js"></script>
             <script src="<?=get_template_directory_uri()?>/assets/bootstrap/js/wow.min.js"></script>
             <script src="<?=get_template_directory_uri()?>/assets/bootstrap/js/scripts.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri()?>/assets/slick/slick.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    
      $('.autoplay').slick({
  slidesToShow: 4,
  slidesToScroll: 4
});     
        $('img.aboutmilestone-img.img-responsive').hover(
        function(){ $(this).removeClass('aboutmilestone-img')},
        function(){ $(this).addClass('aboutmilestone-img')
      });
    });
  
  function getPathFromUrl(url) {
  return url.split("?")[0];
}
  
  //Exhibitor
  $('.filter-select').change(function(e) {
    var url       = getPathFromUrl(window.location.href); 
    var thisval     = $(this).val();
    var orderVal  = $('.order-select').val();
    window.location = url+'?filter='+thisval+'&order='+orderVal;
    
    });
  
  $('.order-select').change(function(e) {
    var thisval     = $(this).val();
    var filterVal = $('.filter-select').val();
    var url       = getPathFromUrl(window.location.href);
    window.location = url+'?filter='+filterVal+'&order='+thisval;
  });
  
  //Schedule
  $('.filter-select-schedule').change(function(e) {
    var url       = getPathFromUrl(window.location.href); 
    var thisval     = $(this).val();
    window.location = url+'?filter='+thisval;
    
    });
  
  //Gif - nogif
  $('.img-nogif').hover(
    function() {
        $(this).addClass("hide");
    $(this).next('.img-gif').removeClass("hide");
    }                      
  );
  
  $( ".img-gif" ).mouseout(function() {
      $(this).addClass("hide");
      $(this).prev('.img-nogif').removeClass("hide");
  });

  
  
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89931599-1', 'auto');
  ga('send', 'pageview');

</script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min.js?ver=4.7.4'></script>
</script>


<script type='text/javascript'>
/* <![CDATA[ */
var countVars = {"disqusShortname":"popcon-asia"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/disqus-comment-system/media/js/count.js?ver=4.7.4'></script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs2.uzone.id/2fn7a2/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582CL4NjpNgssKPAq3HJfREaqfuyAUFFWkDDF9YSBAoiirGpzsJ8WXpP4Vqfz7bAgh2b9O2tpaJq0go%2fl9zWFqtYYigZzMRmD6c0XNid%2fYMzEaxUL3TjklXIvm1eSGXWw6IQcUatZ9wTR%2bXEz4OkP1Qrd2pfdF8EmbshjXxJvEtEJGn7xnYhak1nRdUhzuO6XPbeDOuhEpV3bHPznxHo1rBH9TIFLkCJrUhAW83U5tVpxcVJmsMb%2f6%2bhlQFfIIR16B0zTmBf6UbgkzapsOMWaFCOroXWaUDbDlX4QHsyywRLvEUGBU2B7A8OPl%2bLEqKUVJu7vf6hHP4uz7bylLnys7ZuPpQbf8Dg%2bSUODW3gawRMCBj78qN5%2bsy6RNxNsacQrZfVNxhU8fXKF3M3fmeFlN5A6WljUWiwJuhWi2cUdHAPa6vUFwc9BqbJcMIjcChzxczYPFdlIrPO14" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>