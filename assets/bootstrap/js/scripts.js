//===================
//  Odometer
//  Here set the numebers
//===================

setTimeout(function(){
  $('.odometer.01').html(23578);
}, 1000);
setTimeout(function(){
  $('.odometer.02').html(12702);
}, 1000);


//===================
//  Magnific Popup
//===================

jQuery(document).ready(function() {
  
  jQuery('.image-gallery').each(function() {
  var $container = $(this);
  var $imageLinks = $container.find('.item a');

  var items = [];
  $imageLinks.each(function() {
    var $item = $(this);
    var type = 'image';
    if ($item.hasClass('magnific-youtube')) {
      type = 'iframe';
    }
    var magItem = {
      src: $item.attr('href'),
      type: type
    };
    magItem.title = $item.data('title');    
    items.push(magItem);
    });

  $imageLinks.magnificPopup({
    mainClass: 'mfp-fade',
    items: items,
    gallery:{
        enabled:true,
        tPrev: $(this).data('prev-text'),
        tNext: $(this).data('next-text')
    },
    type: 'image',
    callbacks: {
      beforeOpen: function() {
        var index = $imageLinks.index(this.st.el);
        if (-1 !== index) {
          this.goTo(index);
        }
      }
    }
  });
});
  //===================
  //  WOW
  //  do not touch
  //===================

  new WOW().init();

});



//===================
// Scroller
//  do not touch
//===================

$(function(){

  $('nav.pushy a[href*=#]').click(function() {

  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
      && location.hostname == this.hostname) {

          var $target = $(this.hash);

          $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');

          if ($target.length) {

              var targetOffset = $target.offset().top -0;

              $('html,body').animate({scrollTop: targetOffset}, 800);

              return false;

          }

      }

  });

});
