<?php
  //Template Name: Application Form
get_header();

  // $podsApi = pods_api();
  // $item = $podsApi->load_pod_item(array('pod'=>'submission_modcon'));
  // echo "<pre>";
  // var_dump($item);
?>

<style type="text/css">
  .pods-form-fields{
    list-style-type: none;
  }
  .form-horizontal .control-label{
    text-align: left;
    /*text-indent: 30px;*/
  }
  .pods-file-meta {
    list-style-type: none;
  }
  .pods-file-name input{
    margin-left: -45px;
  }
  .pods-file-icon img{
    margin-top: 40px;
  }
  .bintang{
  	color: #E2595D;
  }
  .controls .pods-form-fields{
  	padding: 0;
  }
  .addfile ul{
  	padding: 0;
  	margin-top: -25px;
  }
</style>
<div style="background-color:#F3F3F3;">
<br><br>
    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                 <h2 class="section-heading" style="color: white; padding: 5px 0px;">
			<small style="color:#FFFFFF; font-size:14px;">Modcon 2017</small><br/><?php echo the_title(); ?>
            </h2>
                </div>
            </div>
        </div>
    </section>
<br>
<div class="container body-single">
<div class="row">
<br>
  <div class="col-lg-8">

                <div style="background-color:#fff;padding:10px 30px; padding-bottom:30px; padding-top:30px;margin-bottom:60px;">


            <form class="form-horizontal" id="formApplication" role="form" action="<?=get_site_url()?>/<?=get_option('modcon_process_form_slug')?>" method="post">
                    <span class="bintang control-label" style="font-style: italic; font-size: 13px;">*) required field</span>

           <h4>PERSONAL DETAIL</h4>
                <div class="form-group">
                	<label for="firstName" class="col-sm-3 control-label">Full Name<span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="fullname" id="firstName" class="form-control" required autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email<span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="email" id="email" name="email" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="mobile_number" class="col-sm-3 control-label">Mobile Number<span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="number" name="mobile_number" id="mobile_number" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="home_number" class="col-sm-3 control-label">Home Number</label>
                    <div class="col-sm-9">
                        <input type="number" id="home_number" name="home_number" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-3 control-label">Address<span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <textarea id="address" class="form-control" required name="address"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="birthDate" class="col-sm-3 control-label">Date of Birth<span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="date" id="birthDate" name="date_of_birth" class="form-control" required placeholder="mm/dd/yyyy">
                    	<small>Date format: mm/dd/yyyy </small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nationality" class="col-sm-3 control-label">Nationality<span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" id="nationality" name="nationality" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="university" class="col-sm-3 control-label">University</label>
                    <div class="col-sm-9">
                        <input type="text" id="university" name="university" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="major" class="col-sm-3 control-label">Major</label>
                    <div class="col-sm-9">
                        <input type="text" id="major" name="major" class="form-control">
                    </div>
                </div>
                <br>
           <h4>IDENTIFICATION</h4>

                <div class="form-group">
                    <label for="passport_number" class="col-sm-3 control-label">Passport Number</label>
                    <div class="col-sm-9">
                        <input type="text" id="passport_number" name="passport_number" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="passport_expiry" class="col-sm-3 control-label">Passport Expiry Date</label>
                    <div class="col-sm-9">
                        <input type="date" id="passport_expiry" name="passport_expiry_date" placeholder="mm/dd/yyyy" class="form-control">
                    	<small>Date format: mm/dd/yyyy </small>
                    </div>
                </div>
                <br>
           <h4>SOCIAL MEDIA</h4>

                <div class="form-group">
                    <label for="instagram_account" class="col-sm-3 control-label">Instagram Account <span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="url" id="instagram_account"  class="form-control" required name="instagram_account">
                    </div>
                </div>
                <div class="form-group">
                    <label for="twitter_account" class="col-sm-3 control-label">Twitter Account <span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="url" id="twitter_account" name="twitter_account"  class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="facebook_account" class="col-sm-3 control-label">Facebook Account <span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="url" id="facebook_account" name="facebook_account"  class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="youtube_account" class="col-sm-3 control-label">Youtube Account <span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="url" id="youtube_account" name="youtube_account" class="form-control" required>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label for="short_bio" class="col-sm-3 control-label">Short Biography<span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <textarea id="short_bio" name="short_bio" class="form-control" required></textarea>
                        <small>Max 75-100 words, , in English</small>
                    </div>
                </div>
                    <?php

      $mypod = pods( 'submission_modcon' );
  $params = array('fields'=>array('self-photo'=> array('label'=>'')),'fields_only'=>true );

    // Output a form with all fields


    ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Self Photo<span class="bintang">*</span></label>
                    <div class="col-sm-9 addfile">
                      <strong><?=$mypod->form($params); ?></strong>
                        <input type="text" id="" name="alt-photo"  class="form-control">
                        <small>In case you can't add file, put your photo URL here</small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="artwork_title" class="col-sm-3 control-label">Artwork Title<span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" id="artwork_title" name="artwork_title"  class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="artwork_desc" class="col-sm-3 control-label">Artwork Description<span class="bintang">*</span></label>
                    <div class="col-sm-9">
                        <textarea id="artwork_desc" name="artwork_desc" class="form-control" required></textarea>
                        <small>Max 100-300 words, in English</small>
                    </div>
                </div>
                    <?php

  $params1 = array('fields'=>array('artwork_file'=> array('label'=>'')),'fields_only'=>true );

    // Output a form with all fields


    ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Artwork File<span class="bintang">*</span></label>
                    <div class="col-sm-9 addfile">
                      <strong><?=$mypod->form($params1); ?></strong>
                        <input type="text" id="" name="alt-artwork"  class="form-control">
                        <small>In case you can't add file, put your artwork URL here. Recommendation: Large video file upload to Youtube/Google Drive and put the link here</small>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="have_you_upload" id="calorieCheckbox" value="yes" required><strong>Have you uploaded this artwork

to your Instagram account

with #AustraliaIndonesia

#ModCon2017 #DigitalArt #AussieBanget ?<span class="bintang"> *</span></strong>
                            </label>
                        </div>
                    </div>
                </div> <!-- /.form-group -->
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="" required><strong>I have read and agree with the following Terms and Conditions of the Competition:<span class="bintang">*</span></strong> <br>
<ul>
<li>You grant the organisers (the Commonwealth of Australia represented by the Australian Embassy Jakarta and ruangrupa Pop Con Asia) the right to maintain digital copies of the Artwork; and reproduce the artworks on the organizers websites and social media platforms such as Facebook, Twitter, Instagram and YouTube.

<li>You warrant that you are the owner of all intellectual property rights for the artwork. Artwork that infringes intellectual property rights will be removed from the competition. You agree to indemnify the organisers against any and all third party claims, liabilities, damages, costs and expenses that may occur as a result of any intellectual property infringement.

<li>The organisers of the Competition may reproduce the artwork in any media format but not limited to postcards, brochures, posters, television/print/digital media, websites, reports, only for the purpose of:

<ul>
<li>Publicity and promotion of the competition.

<li>Event catalogue in current and future years.

<li>Create a non-commercial archive of the competition.
</ul>
</ul>
<strong>The organisers will acknowledge you in all publicity as the creator of the artwork.</strong>
<ul>
<li>If organisers wishes to host an exhibition of selected artworks, the Artist agrees to their work being exhibited free of any charge or fee, where the organisers will fund the reproduction of the artwork.

<li>You acknowledge and agree that the artwork submitted may be reproduced. All reproductions will acknowledge the Artist.

<li>You acknowledge and agree that the artwork uploaded by the organisers to social media digital platforms such as Facebook, Twitter, Instagram and YouTube is subject to additional terms and conditions as stipulated by those platforms.

<li>By entering this competition, you release the organisers from and indemnify the organisers against any and all third party claims, liabilities, damages, costs and expenses arising from or in connection with this competition.

<li>Shortlisted entrants agree to having their artwork, portrait, title of artwork and descriptions, published online.
                            </label>
                        </div>
                    </div>
                </div> <!-- /.form-group -->
                <div class="form-group">
                    <div class="col-sm-12">

                        <input type="submit" class="btn btn-popcon btn-block" value="Submit"><br/><br/>
                        <p>If an error occurs on this form, kindly contact and submit your application  to <a href="mailto:info@popconinc.com">info@popconinc.com</a></p>
                    </div>
                </div>
            </form> <!-- /form -->
      </div>

      </div>
              <div class="col-md-4">

              	<div>

                	<?php
                                    $heroMedia = get_option( 'modcon_hero_media');
                                    $heroMedia = wp_get_attachment_url( $heroMedia[0] );
                                    $heroMediaType = get_option( 'modcon_hero_media_type');
                                    if($heroMediaType=="Image"){
                               ?>
                                <img src="<?=$heroMedia;?>" width="100%"/>
                                <?php } ?>
                </div>

                  <div style="background-color:#fff;">
                  		<ul class="list-group list-modcon">
                         <li class="list-group-item"><a href="<?=get_site_url()."/";?><?=get_option( 'modcon_overview_slug'); ?>">Overview</a></li>
						<?php $submenuId = get_option( 'modcon_submenu_ID');
						$modconMenu = wp_get_nav_menu_items($submenuId);
						foreach($modconMenu as $row):
						 ?>

  <li class="list-group-item"> <a href="<?=$row->url?>"> <?=ucfirst($row->title);?></a></li>
  			<?php endforeach; ?>
</ul>


                  </div>
              </div>
      </div>
        </div> <!-- ./container -->

    </div>


        <!-- Socmed -->
    <section class="modcon-socmed">
    	<div class="container text-center">
        	<div class="row">
            	<?=get_option( 'modcon_social_media_content'); ?>
            </div>
        </div>


    </section>


<script type="text/javascript">
//     $(document).ready(function(){
// 	$('#formApplication').submit(function(e){
// 		if($('input[name*="self-photo"]').length){
// 		}else{
// 			alert('Please complete and upload your CV before submit');

// 		}
// 	});
// }
</script>

<?php
get_footer();

?>