<?php
/*
Template Name: Artist Alley Regular
*/
get_header();

if ( get_query_var('paged') ) {

$paged = get_query_var('paged');

} elseif ( get_query_var('page') ) {

$paged = get_query_var('page');

} else {

   $paged = 1;

}
$filter="";
if(isset($_GET['filter'])){
$filter = $_GET['filter'];
}
$order="";
if(isset($_GET['order'])){
$order = $_GET['order'];
}

$filter = "regular";
$order="smallest-booth-number"

?>

    <br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                 <br><span class="label label-default" >Exhibitor</span>
                <h2 class="section-heading" style="color: white; margin-top:0px; text-transform: uppercase;"><?php echo the_title(); ?></h2>
                </div>
                <div class="col-md-3 hidden-xs">
                 <div class="text-right"><br>
                	<div class="row">
                    	<div class="col-lg-6">
                        	<?php
						$categories = get_terms( 'exhibitor_categories');
						// var_dump($categories);
							?>
                            <select class="form-control filter-select hidden-xs hide">
                    <option value="">No filter</option>
					<?php foreach($categories as $rowCat):
						$select="";
						if($filter==$rowCat->slug){
							$select="selected='selectd'";
						}
					?>
                     <option value="<?=$rowCat->slug;?>" <?=$select?>><?=$rowCat->name;?></option>
                     <?php endforeach; ?>
                </select>
                        </div>
                        <div class="col-lg-6">
                        	<select class="form-control order-select hidden-xs hide">

                                <?php
								
								
																
									$arr = array(
										'az' => 'A-Z',
										'za' => 'Z-A',
										'newest' => 'Newest',
										'oldest' => 'Oldest',
										'smallest-booth-number' => 'Smallest booth number',
										'largest-booth-number' => 'Largest booth number'
									);
								?>

                                <?php foreach($arr as $index=>$value):
									$select="";
									if($order==$index){
										$select="selected='selectd'";
									}
								 ?>
                    				<option <?=$select;?> value="<?=$index?>"><?=$value?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>

                    </div>




                </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
        <div class="row">
              	<div class="col-lg-8" style="margin-bottom:30px;">
                	 <div class="row text-center"><br><br>
            <?php

				

				//echo "<pre>";
				//var_dump($term_children);
				
                $perpage = -1;


				
				$orderArg = array(
					'orderby'  	=> 'meta_value_num',
					'meta_key' 	=> 'booth_number',
					'order' 		=> 'ASC'
				);
				
				$term_id = 173;
				$taxonomy_name = 'exhibitor_categories';
				$term_children = get_term_children( $term_id, $taxonomy_name );
				$term_children2 = get_terms( $taxonomy_name, array( 'child_of' => $term_id ) );
				
				//echo "<pre>";
				//var_dump($term_children2);
				
				foreach ( $term_children2 as $child ) {
				
					$args = array(
						'post_type' => 'exhibitor',
						'posts_per_page' => $perpage,
						'paged'=> $paged,
						'tax_query' => array(
							array(
								'taxonomy' => 'exhibitor_categories',
								'field' => 'id',
								'terms' => $child->term_id
							)
						)
                 	);

					$args = array_merge($args,$orderArg);
				

				//echo "<pre>";
				//var_dump($args); exit;

				 /*

				 */
                $wp_query = new WP_Query($args);
              ?>

              <div class="row">
              	<div class="col-lg-12">
                	<?php
					
                    //$term = get_term_by( 'id', $child, $taxonomy_name );
					echo "<h3>".$child->name."</h3>";
					$pods = pods('exhibitor_categories',$child->term_id);
					$shortname = $pods->field('shortname');
					
					
					?>
                </div>
              
            <?php
				
                while($wp_query->have_posts()) : the_post();

					$pod = pods( 'exhibitor', get_the_id() );
					$booth = $pod->field('booth_number');
					$booth2 = $pod->field('booth_number_2');
                    $alpha_booth = $pod->field('booth_number_3');
                ?>
                <div class="col-md-3 col-xs-6" style="height:250px;">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive img-exhibitor" alt="">
                    <h5 class="service-heading"><?php echo get_the_title(); ?></h4>
                    <p class="text-muted" style="margin-top:-10px;"><?=$shortname.".".$booth.$alpha_booth;?>
                    <?php
					if($booth2){
						echo " - ".$shortname.".".$booth2;
					}?>
                    </p>
                </div>
                <?php endwhile; ?>
                </div>
                
                <?php
				}
				 /*wp_pagenavi();*/ ?>
<div class="col-md-12">
                <br><br>
                </div>
            </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 sidebar" style="margin-top:40px;">
                	<?php echo get_sidebar(); ?>
                </div>

              </div>


        </div>
    </section>



<?php
get_footer();
?>