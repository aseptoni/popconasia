<?php
/*
Template Name: Schedule
*/
get_header();

$title_page = get_the_title();

//Filter parameter
$filter="";
if(isset($_GET['filter'])){
	$filter = $_GET['filter'];
}

//Day list
$day_1 = get_theme_mod("day1");
$day_2 = get_theme_mod("day2");
$day_3 = get_theme_mod("day3");

$day = array(
	$day_1,$day_2,$day_3
);

//Get activities list
$args = array(
		'post_type' => 'activities',
		'orderby'=>'menu_order',
		'order'=>'ASC',
		'tax_query' => array(
			array(
				'taxonomy' 	=> 'activities_categories',
				'field' 		=> 'slug',
				'terms' 		=> 'show-on-schedule'
			)
		)
	 );

$wp_query = new WP_Query($args);

//Recreate activities list
$activitiesSchedule = array();
while($wp_query->have_posts()) : the_post();
	global $post;
	$post_slug=$post->post_name;
	$activitiesSchedule[$post_slug] = get_the_title();
endwhile;

//Conditional filter
$activitiesLoop=array();

if($filter!="all" && $filter!=""){
	foreach($activitiesSchedule as $i=>$v){
		if($i==$filter){
			$activitiesLoop = array(
					$i => $v
			);
		}
	}
}elseif($filter==""){
	$activitiesLoop = array(
		'main-stage' => 'Main Stage'
	);
}else{
	$activitiesLoop = $activitiesSchedule;
}

//var_dump($activitiesLoop);

?>

<br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-xs-12">
                <h2 class="section-heading" style="color: white; padding: 5px 0px;"><?=$title_page;?></h2>
                </div>
                <div class="col-md-3 col-xs-12" style="padding-bottom: 25px;"">
                <div class="text-right"><br><select class="form-control filter-select-schedule">

                    <?php foreach($activitiesSchedule as $iActi=>$vActi):?>
                    	<?php
							$select="";
							$select_all="";
							if($iActi==$filter){
								$select="selected='selected'";
							}elseif($filter=="all"){
								$select_all="selected='selected'";
							}
						?>
                    <option value="<?=$iActi;?>" <?=$select?>><?=$vActi;?></option>
                    <?php endforeach; ?>
                    <option value="all" <?=$select_all;?>>All activities</option>
                </select>
                </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
            <?php

              ?>

              <div class="row">
              	<div class="col-lg-8">
               		<br><br>

					<?php

						foreach($activitiesLoop as $iActLoop=>$vActLoop){

							//DAY 1-3 MAIN STAGE
							$schedule_day = get_terms( 'schedule_day');
							$i=0;
							foreach($schedule_day as $rowDay){
								?>
								<div class="col-lg-12">
									<h3></h3>
								</div>

								<div class="row">
									<div class="col-lg-12">
										<h4 style="text-transform:uppercase; color:#D7182A;"><?=$vActLoop?>: <?=$day[$i];?></h4>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-4"><strong>Time</strong></div>
									<div class="col-lg-4"><strong>Agenda</strong></div>
									<div class="col-lg-4"><strong>Note</strong></div>
								</div>

								<?php
								$i++;

								$perpage = -1;
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								$args = array(
									'post_type' => 'schedule',
									'posts_per_page' => $perpage,
									'paged'=>$paged,
									'orderby'=>'menu_order',
									'order'=>'ASC',
									'tax_query' => array(
										array(
											'taxonomy' 	=> 'schedule_day',
											'field' 		=> 'slug',
											'terms' 		=> 'popcon-day-'.$i.'-schedule'
										)
									)
								 );

								$wp_query = new WP_Query($args);

								while($wp_query->have_posts()) : the_post();
								//Main activities
								$pod = pods( 'schedule', get_the_id() );
								$activities = $pod->field('activities');
								$time_start_1 = $pod->field('time_start_'.$i);
								$time_end_1 = $pod->field('time_end_'.$i);
								$note = $pod->field('note');

								if($activities['post_name']==$iActLoop){ ?>

								<div class="row schedule-item" style="padding-top:5px;
								padding-bottom:5px; border-bottom:1px solid #F1ECEC;">
								<div class="col-lg-4"><?=$time_start_1?> - <?=$time_end_1?></div>
								<div class="col-lg-4"><a href="<?php the_permalink(); ?>"><?=get_the_title()?></a><br>
								<?php if(has_post_thumbnail()){ ?>
								<img src="<?php echo the_post_thumbnail_url('full'); ?>"
								width="150px" class="img-responsive" alt="">
								<?php } //endif thumbnail ?>
								</div>
								<div class="col-lg-4"> <?php if($note){ echo $note;  } ?></div>
								</div><!--end .schedule-item -->

								<?php
								} //endif main-stage
								endwhile;


							}//endforeach
							//END OF DAY 1-3 MAIN STAGE

						}

					?>



                <?php
                //WORKSHOP
			    while($wp_query->have_posts()) : the_post();

					//Day 1Main activities
					$pod = pods( 'schedule', get_the_id() );
					$activities = $pod->field('activities');

					if($activities['post_name']=="workshop-komik"){ ?>
                    <?php echo get_the_title()."<br/>";
					}
					?>

               <?php endwhile; ?>





            <!-- container -->
                </div>





                <div class="col-md-4 col-lg-4 col-sm-12 sidebar"><br>
                	<?php echo get_sidebar(); ?>
                </div>
              </div>

        </div>
    </section>

<?php
get_footer();
?>