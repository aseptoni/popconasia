<?php
/*
Template Name: Experiment
*/

/*
* Terminologi general
* Pods: Artinya ini adalah objek, dapat berupa Custom post type, Taxonomy maupun Advance Custom Post type
*		yang kita buat.
* Pods item: Artinya data/record yang ada di Pods yang dibuat. Bisa kita lihat di All Post/Page/Nama pods kita
*/

/*
* Untuk dapat menggunakan function yang ada di PodsAPI, harus panggil ini dulu
*/
$posdApi = pods_api();

/*
* Parameter untuk menambah Custom post type programaticaly
* detail parameternya bisa dicek di https://github.com/pods-framework/pods/blob/2.x/classes/PodsAPI.php
*/
$defaults = array(
            'create_extend' => 'create',
            'create_pod_type' => 'post_type',
            'create_name' => 'experiment',
            'create_label_singular' => 'Experiment',
            'create_label_plural' => 'Experiments'
        );


/*
* Melakukan pengecekan apakah PODS tersebut sudah ada atau belum
*/
$isExperiemntExist = $posdApi->pod_exists('experiment');

if(!$isExperiemntExist){
	/*
	* Eksekusi menambahkan custom post type
	*/
	$posdApi->add_pod($defaults);
}


/*
* Untuk mengambil data pods, termasuk ID karena dibutuhkan untuk proses selanjutnya
* yaitu proses menambahkan filed pada PODS tersebut
*/
$experiment 		= $posdApi->load_pod('experiment');
$experimentID 	= $experiment['id'];


/*
* Prameter dan eksekusi penambahan satu field
* Jika field sudah ada, maka akan update
*/
$params = array(
			'pod_id'		=> $experimentID,
			'pod'		=> 'experiment',
			'name'		=> 'additional_1',
			'label'		=> 'Additional 1 (Text)',
			'type'		=> 'text',
			'options' 	=> array(
                        	'required' => 1
             			)
		 );
$posdApi->save_field($params);


/*
* Menambahkan satu record ke Pod experiment
*/
$params = array(
	'pod'		=> 'experiment',
	'pod_id'		=> $experimentID,
	'data'		=> array(
		'post_title'		=> 'Posting dari API',
		'post_content'	=> 'Ini adalah konten dari post yang dibuat secara programmaticaly melalui API',
		'additional_1'	=> 'Additional 1 keisi'
	)
);
//$posdApi->save_pod_item($params);


/*
* Menambahkan beberapa record ke Pod experiment / bulk add
*/
$params = array(
	'pod'		=> 'experiment',
	'pod_id'		=> $experimentID
);
$data = array(
	array(
		'post_title'		=> 'Posting dari API 1 ',
		'post_content'	=> 'Ini adalah konten dari post yang dibuat secara programmaticaly melalui API',
		'additional_1'	=> 'Additional 1 keisi',
		'post_status'	=> 'publish'
	),
	array(
		'post_title'		=> 'Posting dari API 2',
		'post_content'	=> 'Ini adalah konten dari post yang dibuat secara programmaticaly melalui API',
		'additional_1'	=> 'Additional 1 keisi',
		'post_status'	=> 'publish'
	),
);
//$posdApi->save_pod_items($params,$data);



/*
* Melakukan pengecekan apakah PODS tersebut sudah ada atau belum
*/
$isExperiemntExist = $posdApi->pod_exists('experiment_category');

if(!$isExperiemntExist){
	
	$param = array(
            'name' => 'experiment_category',
            'label' => 'Experiment Category',
            'type' => 'taxonomy',
			'storage'	=> 'meta',
			'options' => array(
				'hierarchical'	=> 1,
				'built_in_post_types_experiment' => 1 //Supaya otomatis terasosiasi dengan cutom post type tersebut
			),
			'fields' => array(
				array(
				'name'		=> 'additional_tax_1',
				'label'		=> 'Additional 1 (Text)',
				'type'		=> 'text',
				'options' 	=> array(
								'required' => 1
							)
				)
			)
        );
	
	/*
	* Eksekusi menambahkan pod dengan jenis taxonomy / custom taxonomy
	*/
	$posdApi->save_pod($param);
}


/*
	Referensi options
	["options"]=>
  array(38) {
    ["show_in_menu"]=>
    int(1)
    ["built_in_post_types_experiment"]=>
    string(1) "1"
    ["old_name"]=>
    string(19) "experiment_category"
    ["label_singular"]=>
    string(19) "Experiment Category"
    ["public"]=>
    string(1) "1"
    ["hierarchical"]=>
    string(1) "1"
    ["rewrite"]=>
    string(1) "1"
    ["rewrite_with_front"]=>
    string(1) "1"
    ["rewrite_hierarchical"]=>
    string(1) "1"
    ["capability_type"]=>
    string(7) "default"
    ["capability_type_custom"]=>
    string(19) "experiment_category"
    ["query_var"]=>
    string(1) "0"
    ["sort"]=>
    string(1) "0"
    ["built_in_post_types_activities"]=>
    string(1) "0"
    ["built_in_post_types_company"]=>
    string(1) "0"
    ["built_in_post_types_custom_css"]=>
    string(1) "0"
    ["built_in_post_types_customize_changeset"]=>
    string(1) "0"
    ["built_in_post_types_exhibitor"]=>
    string(1) "0"
    ["built_in_post_types_inc_popup"]=>
    string(1) "0"
    ["built_in_post_types_mc4wp-form"]=>
    string(1) "0"
    ["built_in_post_types_page"]=>
    string(1) "0"
    ["built_in_post_types_post"]=>
    string(1) "0"
    ["built_in_post_types_schedule"]=>
    string(1) "0"
    ["built_in_post_types_slider"]=>
    string(1) "0"
    ["built_in_post_types_speaker"]=>
    string(1) "0"
    ["built_in_post_types_attachment"]=>
    string(1) "0"
    ["show_ui"]=>
    string(1) "1"
    ["menu_location"]=>
    string(7) "default"
    ["menu_position"]=>
    string(1) "0"
    ["show_in_nav_menus"]=>
    string(1) "1"
    ["show_tagcloud"]=>
    string(1) "1"
    ["show_admin_column"]=>
    string(1) "0"
    ["pfat_enable"]=>
    string(1) "0"
    ["pfat_append_archive"]=>
    string(6) "append"
    ["rest_enable"]=>
    string(1) "0"
    ["rest_base"]=>
    string(19) "experiment_category"
    ["read_all"]=>
    string(1) "0"
    ["write_all"]=>
    string(1) "0"
	*/

