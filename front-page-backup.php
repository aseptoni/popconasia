<?php
get_header();

// ACTIVITIES SECTION
$judul_act = get_theme_mod("judul_act");
$text_act = get_theme_mod("text_act");
$button_act = get_theme_mod("button_act");
$url_act = get_theme_mod("act_url");

// NEWS SECTION
$judul_news = get_theme_mod("judul_news");
$button_news = get_theme_mod("button_news");
$url_news = get_theme_mod("news_url");

// WHO'S COMING SECTION
$judul_who = get_theme_mod("judul_who");
$text_who = get_theme_mod("text_who");
$button_who = get_theme_mod("button_who");
$url_who = get_theme_mod("who_url");

// SPONSOR & PARTNER
$judul_snp = get_theme_mod("judul_snp");
$judul_sponsor = get_theme_mod("judul_sponsor");
$judul_partner = get_theme_mod("judul_partner");

?>
    <!-- Slider Desktop -->
    <div class="visible-lg">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-top: 5px;">

        <?php $args = array(
				'post_type' => 'slider',
				'tax_query' => array(
					array(
					'taxonomy' => 'slider_type',
					'field' => 'slug',
					'terms' => 'desktop'
					)
					)
			);
			$wp_query = new WP_Query($args);
		?>

      <!-- Indicators -->
	  <ol class="carousel-indicators">

		  <?php
		   $hitung = 0;
		  while($wp_query->have_posts()) :

            the_post();
			 $active="";
			 if ($hitung == 0) {
				  $active="active";
			 }
			?>
	    <li data-target="#carousel-example-generic" data-slide-to="<?=$hitung?>" class="<?=$active?>"></li>
        <?php
		$hitung = $hitung + 1;
		endwhile; ?>
	  </ol>
        <br><br>
        <div class="carousel-inner" role="listbox">

            <?php
           // var_dump($wp_query);
            $hitung = 0;
            while($wp_query->have_posts()) :
            the_post();
            $hitung = $hitung + 1;
            //var_dump($wp_query);
            if ($hitung == 1) {

            ?>
            <div class="item active">
                <img src="<?php echo the_post_thumbnail_url(); ?>" >
            </div>
            <?php
                }
                else {
            ?>
                <div class="item">
                    <img src="<?php echo the_post_thumbnail_url(); ?>" >
                </div>
            <?php }; endwhile; ?>
        </div>

	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>
    </div>

    <!-- Slider Mobile -->
    <div class="hidden-lg">
    <div id="carousel-example-generic-mobile" class="carousel slide" data-ride="carousel" style="margin-top: -10px;">
      <!-- Indicators -->
      <!-- <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic-mobile" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic-mobile" data-slide-to="1"></li>
      </ol> -->
        <br><br><br>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <?php $args = array(
              'post_type' => 'slider',
              'tax_query' => array(
			  			array(
                                    'taxonomy' => 'slider_type',
                                    'field' => 'slug',
                                    'terms' => 'mobile'
                            )
							)
            );
            $wp_query = new WP_Query($args);
            $hitungm = 0;
            while($wp_query->have_posts()) :
            the_post();
            $hitungm = $hitungm + 1;
            //var_dump($wp_query);
            if ($hitungm == 1) {

            ?>
            <div class="item active">
                <img src="<?php echo the_post_thumbnail_url(); ?>" >
            </div>
            <?php
                }
                else {
            ?>
                <div class="item">
                    <img src="<?php echo the_post_thumbnail_url(); ?>" >
                </div>
            <?php }; endwhile; ?>
      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic-mobile" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    </div>

    <!-- Activities Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading"><?= $judul_act; ?></h2>
                    <br>
                </div>
            </div>
            <?php
                $perpage = 3;
                $args = array( 'post_type' => 'activities', 'posts_per_page' => $perpage,
                 'tax_query' => array(
                      array(
                        'taxonomy' => 'activities_categories',
                        'field'    => 'slug',
                        'terms'    => 'show-on-home'
                      )));
                $wp_query = new WP_Query($args);
              ?>
            <div class="row text-center">
            <?php
                while($wp_query->have_posts()) : the_post();
                 $pod = pods( 'activities', get_the_id() );
                    $desk = $pod->field('deskipsi_singkat');
					$picture_no_gif = $pod->field('picture_no_gif');
					?>
					<pre class="hide">
					<?php
                    var_dump($picture_no_gif);
                ?>
                </pre>
                <div class="col-md-4 col-lg-4 col-sm-12 konten"><a href="<?php the_permalink(); ?>">
                    <div class="visible-lg">
                	<img src="<?php echo $picture_no_gif['guid']; ?>" class="img-nogif img-responsive" alt="">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-gif img-responsive hide" alt="">
                    </div>
                    <div class="hidden-lg">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="" style="width: 100%;">
                    </div>
                    </a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;">
                    <h4 class="service-heading" style="display:none;"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted" style="margin-top:5px;"><?php
                        if (strlen($desk) > 70)
                        $desk = substr($desk, 0, 70) . '...';
                    echo $desk; ?></p>
                </div>
                 <?php endwhile; ?>
            </div>
        </div>
            <div class="col-lg-12 col-md-12 col-xs-12 text-center">
          	<!-- <p><? /*$text_act;*/ ?></p> -->
            <a href="<?php echo $url_act; ?>"><button class="btn btn-popcon"><?= $button_act ?></button></a><br><br><br>
        </div>
    </section>

    <!-- Who Coming Section -->
<!--
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading"><?= $judul_who ?></h2>
                    <br>
                </div>
            </div>
            <?php
               /* $perpage = 4;
                $args = array( 'post_type' => 'speaker', 'posts_per_page' => $perpage,
                 'tax_query' => array(
				      array(
				        'taxonomy' => 'speaker_categories',
				        'field'    => 'slug',
				        'terms'    => 'show-on-home'
				      )));
                $wp_query = new WP_Query($args);
              ?>
            <div class="row text-center">
            <?php
                while($wp_query->have_posts()) : the_post();
					$pod = pods( 'speaker', get_the_id() );
                   $jabatan = $pod->field('jabatan');
					$organisasi = $pod->field('organisasi');
                ?>
                <div class="col-md-3 col-lg-3 col-xs-6 konten"><a href="<?php the_permalink(); ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;"><h4 class="service-heading"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted"><?php echo $jabatan;

					if($organisasi){
					 echo ", ".$organisasi;
					}

					?></p>
                </div>
                 <?php endwhile; ?>
            </div>
        </div>
            <div class="col-lg-12 col-md-12 col-xs-12 text-center">
            <h5><?= $text_who; ?></h5>
            <br>
            <a href="<?php echo $url_who; */ ?>"><button class="btn btn-popcon"><?= $button_who; ?></button></a><br><br><br>
        </div>
    </section>
-->
    <!-- News -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading"><?= $judul_news; ?></h2>
                    <br>
                </div>
            </div>
            <?php
                $perpage = 4;
                $args = array( 'post_type' => 'post', 'posts_per_page' => $perpage,
                 'tax_query' => array(
                      array(
                        'taxonomy' => 'post_on_home',
                        'field'    => 'slug',
                        'terms'    => 'yes'
                      )));
                $wp_query = new WP_Query($args);
              ?>
            <div class="row text-center">
            <?php
                while($wp_query->have_posts()) : the_post();
                $desk = get_the_excerpt();
                ?>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 konten"><a href="<?php the_permalink(); ?>">
                    <!-- <img class="img-responsive"> src="<?php echo the_post_thumbnail_url('full'); ?>"> -->
                    <div style="background-image: url('<?php echo the_post_thumbnail_url('full'); ?>'); background-size: cover; height: 170px;"></div>
                    </a><br>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;"><h4 class="service-heading" style="height: 60px;"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted"><?php
                        if (strlen($desk) > 140)
                        $desk = substr($desk, 0, 140) . '...';
                    echo $desk; ?></p>
                </div>
                 <?php endwhile; ?>
            </div>
        </div>
            <div class="col-lg-12 col-md-12 col-xs-12 text-center">
            <a href="<?php echo $url_news; ?>"><button class="btn btn-popcon"><?= $button_news ?></button></a><br><br><br>
        </div>
    </section>

    <!-- Sponsor Partner Section -->
    <center>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading"><?= $judul_snp; ?></h2>
                    <br>
                </div>
            </div>
            <?php
                        $categories = get_terms( array(
							'taxonomy'=>'categories_company',
							'orderby'=>'slug'
						));
                        // var_dump($categories);
                        foreach($categories as $rowCat):
                          ?>
                      <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h3 class="section-heading"><?=$rowCat->name;?></h3>
                    <br>
            <?php
            $slug=$rowCat->slug;

                $perpage = 6;
                $args = array( 'post_type' => 'Company',
				'orderby'  => 'title',
                  		'order'    => 'ASC',
                'tax_query' => array(
                      array(
                        'taxonomy' => 'categories_company',
                        'field'    => 'slug',
                        'terms'    =>  $slug
                      )));
                $wp_query = new WP_Query($args);
              ?>
            <div class="row row-centered text-center">
                <?php
                while($wp_query->have_posts()) : the_post();
                $pod = pods( 'Company', get_the_id() );

                $link_company = $pod->field( 'link_company' );
				if($slug=="asponsor"){
					$collg="col-lg-2";
				}else{
					$collg="col-lg-2";
				}
                ?>
                <div class="col-centered col-md-2 col-xs-6 <?=$collg;?> konten"><a target="_blank" href="<?php echo $link_company; ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                </div>
                <?php endwhile; ?>
            </div>
                </div>
            <br>
                     <?php endforeach; ?>


        </div>
    </section>
</center>
<?php get_footer(); ?>