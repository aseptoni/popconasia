<?php
get_header();

$pod = pods( 'schedule', get_the_id() );
$idna = get_the_id();
$people = $pod->field('people');
$company = $pod->field('company');
$exhibitor = $pod->field('exhibitor');
?>


<br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <br><a href="http://dev.popconasia.com/schedule/"><span class="label label-default" >Schedule</span></a>
            <h2 class="section-heading" style="color: white; margin-top:0px; text-transform: uppercase;">
                <?php echo get_the_title(); ?>
            </h2>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
            <div class="row"><br>
              <div class="col-md-8 col-lg-8 col-sm-12">
              <!-- Detail Information -->
                <div class="panel panel-default">
                <div class="panel-heading"><h5>DETAIL INFORMATION</h5></div>
                <div class="panel-body">
                <div class="row">
                <?php
                  if (has_post_thumbnail()) {
                   ?>
                    <div class="col-md-4 col-lg-4">
                  <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%;" >
                  </div>
                  <div class="col-md-8 col-lg-8">
                    <p>  <?php the_post(); the_content(); ?> </p>

                  </div>
                   <?php
                  }else{
                ?>
                  <div class="col-md-12 col-lg-12">
                    <p>  <?php the_post(); the_content(); ?> </p>

                  </div>
                  <?php
                    }
                  ?>
                  </div>
                  </div>
                  </div>
<br><br>
              <?php
$people = $pod->field( 'people' );
if ( ! empty( $people ) ) {
?>
 <!-- people -->
                  <div class="panel panel-default">
                    <div class="panel-heading"><h5>SPEAKER</h5></div>
                    <div class="panel-body">
                      <div class="row text-center"><br><br>
                       <?php
  foreach ( $people as $rel ) {
    $id = $rel[ 'ID' ];
    ?>
                          <div class="col-md-3 col-lg-3 col-sm-6"><a href="<?php echo get_the_permalink($id); ?>">
                              <img src="<?php echo get_the_post_thumbnail_url($id); ?>" class="img-responsive img-circle" alt="" ></a>
                              <h4 class="service-heading"><?php echo get_the_title($id); ?></h4>
                              <p class="text-muted"><?php get_the_excerpt($id); ?></p>
                          </div>
<?php
    }
?>
                      </div>
                    </div>
                  </div>
              <!-- end speaker -->
<?php
}
?>

<?php
$exhibitor = $pod->field( 'exhibitor' );
if (! empty( $exhibitor )) {
?>
<!-- Exhibitor -->
                  <div class="panel panel-default">
                    <div class="panel-heading"><h5>EXHIBITOR</h5></div>
                    <div class="panel-body">
                      <div class="row text-center"><br><br>
                    <?php
  foreach ( $exhibitor as $rel ) {
    $id = $rel[ 'ID' ];
    ?>

                          <div class="col-md-3 col-lg-3 col-sm-6"><a href="<?php echo get_the_permalink($id); ?>">
                              <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                              <h4 class="service-heading"><?php echo get_the_title($id); ?></h4>
                              <p class="text-muted">Web</p>
                          </div>
<?php
    }

?>
                      </div>
                    </div>
                  </div><!-- end exhibitor -->
<?php
}
?>

<?php
$company = $pod->field( 'company' );
if (! empty( $company )) {
?>
<!-- company -->
                  <div class="panel panel-default">
                    <div class="panel-heading"><h5>COMPANY</h5></div>
                    <div class="panel-body">
                      <div class="row text-center"><br><br>
                    <?php
  foreach ( $company as $rel ) {
    $id = $rel[ 'ID' ];
    ?>

                          <div class="col-md-3 col-lg-3 col-sm-6"><a href="<?php echo get_the_permalink($id); ?>">
                              <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                              <h4 class="service-heading"><?php echo get_the_title($id); ?></h4>
                              <p class="text-muted">Web</p>
                          </div>
<?php
    }

?>
                      </div>
                    </div>
                  </div><!-- end company -->
<?php
}
?>

<!-- More schedule -->
            <?php
            $perpage = 4;
            $the_query = query_posts(
            array(
            'post__not_in' => array($idna),
            'post_type'=>'schedule',
            'posts_per_page'=>$perpage,
            'paged'=>$paged,
            'orderby'=>'rand'
            )
            );
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            // $wp_query = new WP_Query($args);
            ?>
            <div class="panel panel-default">
            <div class="panel-heading"><h5>MORE SCHEDULE</h5></div>
            <div class="panel-body">
            <ul>
            <?php
            while($wp_query->have_posts()) : the_post();
            ?>
            <li>
            <a href="<?php the_permalink(); ?>"><h5 class="service-heading"><?php echo get_the_title(); ?></h5></a>
            </li>
            <?php endwhile; ?>
            </ul>
            </div>
            </div>
      <!-- End schedule -->

              </div>
               <div class="col-md-4 col-lg-4 col-sm-12 sidebar">
                  <?php
                    get_sidebar();
                  ?>
              </div>




            </div>
        </div>
    </section>

<?php
    get_footer();
?>