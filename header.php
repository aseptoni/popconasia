<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

<?php

 wp_head();

?>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />

  <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php bloginfo( 'name' ); ?></title>

    <?php

	$footer_desc = get_theme_mod("footer_desc");

	?>


    <link href="<?php bloginfo( 'template_directory' ); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript">

        $(document).ready(function(){

            $('.carousel').carousel();

            // $('.ninja-forms-field').addClass("form-control");

        	$('#myModal').on('shown.bs.modal', function () {

  			$('#myInput').focus()

			})

        });



    </script>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_directory' ); ?>/style.css?v=16">

      <link type="text/css" rel="stylesheet" href="<?=get_template_directory_uri()?>/slick/slick.css"  media="screen,projection"/>

      <link type="text/css" rel="stylesheet" href="<?=get_template_directory_uri()?>/slick/slick-theme.css"  media="screen,projection"/>

<!--<script type="text/javascript" src="<?=get_template_directory_uri()?>/slick/slick.min.js"></script>-->

<style type="text/css">

body{

	font-size: 15px !important;
}

.modal-backdrop.in {

	z-index: 500;

}

#bs-example-navbar-collapse-1{

  text-align: center;

}

</style>

<!-- <script id="socital-script" src="https://plugin.socital.com/static/v1/socital.js" data-socital-user-id="5873564256b15e4bbab23564"></script> -->

</head>

<?php

$logo = get_theme_mod("img-upload");

 ?>

<body>

<div id="fb-root"></div>

<script>(function(d, s, id) {

  var js, fjs = d.getElementsByTagName(s)[0];

  if (d.getElementById(id)) return;

  js = d.createElement(s); js.id = id;

  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=199086303495070";

  fjs.parentNode.insertBefore(js, fjs);

}(document, 'script', 'facebook-jssdk'));</script>

    <!-- Navigation -->

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" style="background-color: white; height:52px;">

        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header page-scroll">

                <a class="right hidden-lg" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="position: absolute; right: 5%; top: 30%;">

                    <img class="" src="https://popconasia.com/wp-content/uploads/2017/01/icon-burger.png" style="width: 25px; ">

                </a>

                <a class="navbar-brand hidden-xs hidden-sm" href="https://popconasia.com/" style="padding:10px;"><img src="<?php echo $logo; ?>" style="height:35px"></a>

                <a class="navbar-brand hidden-md hidden-lg" href="https://popconasia.com/"><img src="<?php echo $logo; ?>" style="height:25px"></a>

            </div>





            <!-- Collect the nav links, forms, and other content for toggling -->

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background-color: white; height:0px !important;">

       <?php

                        $buy_label = get_theme_mod("buy_label");

                        $buy_url = get_theme_mod("buy_url");

                     ?>

<?php

 $menu_args = array(

  'menu'    => 'my-menu',

  'theme_location' => 'my-menu',

  'depth'    => 4,

  'container'   => false,

  'menu_class'   => 'nav navbar-nav navbar-right',

  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',

  'walker'    => new wp_bootstrap_navwalker()

 );

 wp_nav_menu($menu_args);

?>

            </div>

            <!-- /.navbar-collapse -->

        </div>

        <!-- /.container-fluid -->



<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">

  <div class="modal-dialog modal-md" role="document">

    <div class="modal-content">

      <div class="modal-body">

        <form method="get" id="searchform" action="<?php bloginfo('url'); ?>">

        <div class="input-group">

        <input type="text" class="form-control" name="s" id="s" placeholder="Search for..." autofocus>

        <input type="hidden" name="search-type" value="normal" />

        <span class="input-group-btn">

          <input class="btn btn-default" type="submit" value="Go!!" name="submit">

          </span>

      </div><!-- /input-group -->

      </form>

      </div>

    </div>

  </div>

</div>

    </nav>

