<?php
get_header();

?>

<style type="text/css">

  .inicontent img, .inicontent figure{
    width: 100% !important;
    height: auto;
  }
   .inicontent iframe{
    width: 100%;
    height: 512px;
   }
</style>

<br><br>

    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <br><a href="http://dev.popconasia.com/blog/"><span class="label label-default" >Blog</span></a>
            <h2 class="section-heading" style="color: white; margin-top:0px; text-transform: uppercase;">
                <?php echo get_the_title(); ?>
            </h2>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container body-single">
            <div class="row"><br>
              <div class="col-md-8">
                  <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="">
                  <br>
                  <div class="inicontent" style="font-size: 17px; line-height: 1.6;"><?php
        echo '<p>Posted on '.get_the_date('M d, Y').'</p>';
        the_post();
    the_content();
        ?>
                  </div>

                <?php comments_template(); ?>

              </div>
              <div class="col-md-4 col-lg-4 col-sm-12 sidebar">
                  <?php

                    $idna = get_the_id();
                    get_sidebar();

                  ?>
              </div>
        </div>
    </section>
<?php get_footer(); ?>