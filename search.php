<?php 
get_header();

?>

   <br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-8">    
                <h2 class="section-heading" style="color: white; text-transform: uppercase;">
                 <?php echo "Result of : ";  the_search_query(); ?></h2>
                </div>
            </div>
        </div>
    </section>

<section>
        <div class="container"><br>
            <div class="row">
                <div class="col-md-8">
<?php
$args = array( 'post_type' => 'post','posts_per_page' => 5 ); 
$args = array_merge( $args, $wp_query->query ); 
query_posts( $args );?>

<?php if ( have_posts() ) : ?>
                <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading">NEWS</h2>
                    <br>
                </div>
                </div>
<?php while ( have_posts() ) : the_post(); ?>


<div class="panel panel-default">
        <div class="panel-body">                    
            <div class="row">
                        <div class="col-lg-3 col-md-3 col-xs-12 hidden-lg" style="background-image: url('<?php echo the_post_thumbnail_url('full'); ?>'); background-size: cover; height: 150px; margin-left: 15px; background-position: center; width: 250px;">
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12 visible-lg" style="background-image: url('<?php echo the_post_thumbnail_url('full'); ?>'); background-size: cover; height: 150px; margin-left: 15px; background-position: center; width: 150px;">
                        </div>
                        <div class="col-lg-9 col-md-9 col-xs-12">
                            <a style="font-size:25px; color:black;" href="<?php the_permalink(); ?>"><h4 class="judul-blog"><?php the_title(); ?></h4></a>
        <?php echo '<p>Posted on '.get_the_date('M d, Y').'</p>'; ?>
        <p><?php the_excerpt(); ?></p><a style="color:#CC1D22;" href="<?php the_permalink(); ?>">Baca Selengkapnya</a>

                        </div>
            </div>
        </div>
    </div>
<?php endwhile;?>

        

<?php endif;?>


<center>
                <?php wp_pagenavi(); ?>
                </center>
<?php
wp_reset_query();
wp_reset_postdata();
?>
<?php
$args = array( 'post_type' => 'activities','posts_per_page' => 6 ); 
$args = array_merge( $args, $wp_query->query ); 
query_posts( $args );?>

<?php if ( have_posts() ) : ?>
                <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading">ACTIVITIES</h2>
                    <br>
                </div>
                </div>
<?php while ( have_posts() ) : the_post(); ?>


<div class="col-md-4 col-lg-4 col-xs-12 konten text-center"><a href="<?php the_permalink(); ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;"><h4 class="service-heading"><?php echo get_the_title(); ?></h4></a>
                </div>
<?php endwhile;?>


        

<?php endif;?>


<center>
                <?php wp_pagenavi(); ?>
                </center>

 <?php
wp_reset_query();
wp_reset_postdata();
?>
<?php
$args = array( 'post_type' => 'speaker','posts_per_page' => 8 ); 
$args = array_merge( $args, $wp_query->query ); 
query_posts( $args );?>

<?php if ( have_posts() ) : ?>
                <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading">SPEAKER</h2>
                    <br>
                </div>
                </div>
<?php while ( have_posts() ) : the_post(); ?>

<div class="col-md-3 col-lg-3 col-xs-12 konten text-center"><a href="<?php the_permalink(); ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;"><h4 class="service-heading"><?php echo get_the_title(); ?></h4></a>
                    <!-- <p class="text-muted"><?php the_excerpt(); ?></p> -->
                </div>
<?php endwhile;?>




      
<?php endif;?>


<center>
                <?php wp_pagenavi(); ?>
                </center> 

 <?php
wp_reset_query();
wp_reset_postdata();
?>
<?php
$args = array( 'post_type' => 'exhibitor','posts_per_page' => 12 ); 
$args = array_merge( $args, $wp_query->query ); 
query_posts( $args );?>


<?php if ( have_posts() ) : ?>
                <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                    <h2 class="section-heading">EXHIBITOR</h2>
                    <br>
                </div>
                </div>
<?php while ( have_posts() ) : the_post(); 

	$pod = pods( 'exhibitor', get_the_id() );
					$booth = $pod->field('booth_number');
                ?>   
                <div class="col-md-2 text-center"><a href="<?php the_permalink(); ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;">
                    <h4 class="service-heading"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted" style="margin-top:-10px;"><?=$booth;?></p>
                </div>
<?php endwhile;?>




      
<?php endif;?>


<center>
                <?php wp_pagenavi(); ?>
                </center>
                    </div>

            <div class="col-md-4">
                  <?php
                    get_sidebar();
                  ?>
              </div>
                </div>
            </div>
        </div>
    </section>

    

<?php
get_footer();
?>