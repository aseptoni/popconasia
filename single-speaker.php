<?php
get_header();

$pod = pods( 'speaker', get_the_id() );
$idna = get_the_id();
$link = $pod->field('links');
$link_label = $pod->field('link_label');
$jabatan = $pod->field('jabatan');
$organisasi = $pod->field('organisasi');
?>


<br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
        	<br><a href="http://dev.popconasia.com/whos-coming/"><span class="label label-default" >Who's Coming</span></a>
            <h2 class="section-heading" style="color: white; margin-top:0px; text-transform: uppercase;"><?php echo get_the_title(); ?></h2>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
            <div class="row"><br>
              <div class="col-md-8 col-lg-8 col-sm-12">

                <div class="panel panel-default">
                <div class="panel-heading"><h5>DETAIL INFORMATION</h5></div>
                <div class="panel-body">

                <div class="row">
                  <div class="col-md-4 col-lg-4">
                  <img src="<?php echo the_post_thumbnail_url(); ?>" style="width: 100%;" ><br><br>
                  <?php
				  	if($link){
				  ?>
                  <a href="<?php echo $link; ?>"><button class="btn btn-popcon"><?=$link_label?></button></a>
                  <?php } ?>
                  </div>
                  <div class="col-md-8 col-lg-8">
                  	<h5><?php echo $jabatan;

					if($organisasi){
					 echo ", ".$organisasi;
					}
					  ?></h5>
                    <p>  <?php the_post(); the_content(); ?> </p>

                  </div>

                  </div>
                  </div>
                  </div>



              <?php
$activities = $pod->field( 'activities' );
if ( ! empty( $activities ) ) {
?>
 <!-- activities -->
                  <div class="panel panel-default">
                    <div class="panel-heading"><h5>ACTIVITIES</h5></div>
                    <div class="panel-body">
                      <div class="row text-center">
                       <?php
  foreach ( $activities as $rel ) {
    $id = $rel[ 'ID' ];
    ?>
                          <div class="col-md-3 col-lg-3 col-sm-6"><a href="<?php echo get_the_permalink($id); ?>">
                              <img src="<?php echo get_the_post_thumbnail_url($id); ?>" class="img-responsive" alt="" ></a>
                               <a href="<?php echo get_the_permalink($id); ?>"><h5 class="service-heading"><?php echo get_the_title($id); ?></h5></a>

                          </div>
<?php
    }
?>
                      </div>
                    </div>
                  </div>
              <!-- end activities -->
<?php
}
?>

<!-- Exhibitor / Company -->
            <?php
            $exhibitor = $pod->field( 'exhibitor' );
            $company = $pod->field( 'company' );
            if (!empty( $exhibitor ) || !empty( $company )) {
            ?>
            <div class="panel panel-default">
            <div class="panel-heading"><h5>EXHIBITOR / COMPANY</h5></div>
            <div class="panel-body">
            <div class="row text-center">
            <?php

            //Looping exhibitor
			if($exhibitor){
            foreach ( $exhibitor as $rel ) {
            $id = $rel[ 'ID' ];
            ?>
            <div class="col-md-3 col-lg-3 col-xs-6"><a href="<?php echo get_the_permalink($id); ?>">
            <?php
                    if (has_post_thumbnail()) {
                   ?>
            <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="">
              <?php }else{  ?>
              <img src="http://dev.popconasia.com/wp-content/uploads/2017/01/Untitled-2-01.png" class="img-responsive" alt="">
              <?php } ?>
            </a>
            <a href="<?php echo get_the_permalink($id); ?>"><h5 class="service-heading"><?php echo get_the_title($id); ?></h5></a>
            </div>
            <?php } } //end Looping exhibitor ?>


            <?php
            //Looping company
			if($company){
            foreach ( $company as $rel ) {
            $id = $rel[ 'ID' ];
            ?>
            <div class="col-md-3 col-lg-3 col-xs-6">
            <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="">
            <a href="<?php echo get_the_permalink($id); ?>"><h5 class="service-heading"><?php echo get_the_title($id); ?></h5></a>
            </div>
            <?php  } }   //end Looping company ?>

            </div>
            </div>
            </div>
            <?php }

			 ?>
            <!-- End Exhibitor / Company -->

            <!-- More speaker -->
            <?php
            $perpage = 4;
            $the_query = query_posts(
            array(
            'post__not_in' => array($idna),
            'post_type'=>'speaker',
            'posts_per_page'=>$perpage,
            'paged'=>$paged,
            'orderby'=>'rand'
            )
            );
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            // $wp_query = new WP_Query($args);
            ?>
            <div class="panel panel-default">
            <div class="panel-heading"><h5>MORE POPSTARS</h5></div>
            <div class="panel-body">
            <div class="row text-center">
            <?php
            while($wp_query->have_posts()) : the_post();
			$pod = pods( 'speaker', get_the_id() );
                    $jabatan = $pod->field('jabatan');
					$organisasi = $pod->field('organisasi');
            ?>
            <div class="col-md-3 col-lg-3 col-xs-6 konten"><a href="<?php the_permalink(); ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;">
                    <h4 class="service-heading"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted"><?php echo $jabatan; if($organisasi){ echo ", ".$organisasi; } ?></p>
                </div>
            <?php endwhile; ?>
            </div>
            </div>
            </div>
      <!-- End speaker -->
              </div>
               <div class="col-md-4 col-lg-4 col-sm-12 sidebar">
                  <?php
                    get_sidebar();
                  ?>
              </div>




            </div>
        </div>
    </section>

<?php
    get_footer();
?>