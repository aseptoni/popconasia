<?php
/*
Template Name: Activities
*/
get_header();


$filter="";
if(isset($_GET['filter'])){
$filter = $_GET['filter'];
}
$order="";
if(isset($_GET['order'])){
$order = $_GET['order'];
}
?>

<br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-xs-12">    
                <h2 class="section-heading" style="color: white; text-transform: uppercase;"><?php echo the_title(); ?></h2>
                </div>
                <div class="col-md-3 col-xs-12" style="padding-bottom: 25px;">
                 <div class="text-right"><br>
                    
                            <select class="form-control order-select">
                                
                                <?php
                                    $arr = array(
                                        'newest' => 'Newest',
                                        'oldest' => 'Oldest',
                                        'az' => 'A-Z',
                                        'za' => 'Z-A'

                                    );
                                ?>
                                
                                <?php foreach($arr as $index=>$value):
                                    $select="";
                                    if($order==$index){
                                        $select="selected='selectd'";
                                    }
                                 ?>
                                    <option <?=$select;?> value="<?=$index?>"><?=$value?></option>
                                <?php endforeach; ?>
                               
                            </select>
                
                </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
            <?php

                $perpage = 8;
                if($order=="newest"){
                    $orderArg = array(
                        'orderby'   => 'date',
                        'order'         => 'DESC'
                    );
                }elseif($order=="oldest"){
                    $orderArg = array(
                        'orderby'   => 'date',
                        'order'         => 'ASC'
                    );
                }elseif($order=="az"){
                    $orderArg = array(
                        'orderby'   => 'title',
                        'order'         => 'ASC'
                    );
                }elseif($order=="za"){
                    $orderArg = array(
                        'orderby'   => 'title',
                        'order'         => 'DESC'
                    );
                }else{
                    $orderArg = array(
                        'orderby'   => 'date',
                        'order'         => 'DESC'
                    );
                }
                
                    $args = array(
                    'post_type' => 'activities',
                    'posts_per_page' => $perpage,
                    'paged'=> $paged
                    );
                    $args = array_merge($args,$orderArg);
                
                $wp_query = new WP_Query($args);

        ?>
            <div class="row text-center"><br><br>
            <?php
                while($wp_query->have_posts()) : the_post();
                $pod = pods( 'activities', get_the_id() );
                    $desk = $pod->field('deskipsi_singkat');
					$picture_no_gif = $pod->field('picture_no_gif');
                ?>   
                <div class="col-md-3 col-lg-3 col-xs-12 konten"><a href="<?php the_permalink(); ?>">
<img src="<?php echo $picture_no_gif['guid']; ?>" class="img-nogif img-responsive" alt="">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-gif img-responsive hide" alt="">                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;">
                    <h4 class="service-heading"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted" style="height: 20px;"><?php 
                        if (strlen($desk) > 70)
                        $desk = substr($desk, 0, 70) . '...';
                    echo $desk; ?></p>
                </div>
                <?php endwhile; ?>
                
                <div class="col-md-12">
                
                <?php wp_pagenavi(); ?>
                
                </div>
            </div><!-- container -->
        </div>
    </section>

<?php
get_footer();
?>