<?php
get_header();
?>


<br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <h2 class="section-heading" style="color: white; padding: 5px 0px;"><?php echo the_title(); ?></h2>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container body-single">
            <div class="row"><br>
              <div class="col-md-8">

                  <br>
                  <p style="text-align: justify;"><?php
        // echo '<p>Posted on '.get_the_date('M d, Y').'</p>';
        the_post();
    the_content();
        ?>
                  </p>
                  <!-- <button class="btn btn-popcon">SHARE</button> -->
                  <?php comments_template(); ?>

              </div>
              <div class="col-md-4 col-lg-4 col-sm-12 sidebar">
                      <?php
                      get_sidebar();
                      ?>
              </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>

