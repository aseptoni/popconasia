<?php
/*
Template Name: Gallery Sidebar
*/
get_header();

?>

<br><br>


<!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <h2 class="section-heading" style="color: white;"><?php echo get_the_title(); ?></h2>
                </div>
               
            </div>
        </div>
    </section>
    
    <!-- Body Section -->
    <section>
        <div class="container">
        		
                <div class="row">
                    <div class="col-lg-8" style="margin-top:13px;">
                        
                        <?php
							$categories = get_terms( array(
							'taxonomy'=>'year',
							'orderby'=>'slug',
							'order'	=> 'DESC'
							));
							// var_dump($categories);
							foreach($categories as $rowCat):
							$slug=$rowCat->slug;
							if($slug!="2017"){
						?>
                        
                        
                        <div class="row">
                        		
                            <div class="col-lg-12">
                            	<h2 class="section-heading"><?=$rowCat->name;?></h3>
                            </div>
                        
                        	<?php
								
								$args = array( 'post_type' => 'gallery',
								 'orderby'   => 'title',
								  'order'         => 'ASC',
								  'tax_query' => array(
									  array(
										'taxonomy' => 'year',
										'field'    => 'slug',
										'terms'    =>  $slug
									  )
									)
								 );
				
								$wp_query = new WP_Query($args);
								
               				 while($wp_query->have_posts()) : the_post();
							 $pod = pods( 'gallery', get_the_id() );
								$subtitle = $pod->field('subtitle');
							?>
                        
                        	<div class="col-lg-6" style="margin-bottom:30px;">
                            	<a href="<?php the_permalink(); ?>">
                            	<img src="<?php echo the_post_thumbnail_url('medium'); ?>" width="100%" class="img-responsive" alt="">	
                                	<div style="padding:10px 20px; background-color:#FAFAFB; color:rgba(35,35,35,1.00)">
                                    <h4 style="margin-bottom:0px;"><?php echo get_the_title(); ?></h4>
                                <p><?=$subtitle?></p>
                                    </div>						
                                
                                </a>
                            </div>
                            
                              <?php endwhile; ?>
                            
                        </div>
                        <?php 
							}
						endforeach; ?>
                        
                    </div>
                    
                    <div class="col-md-4 col-lg-4 col-sm-12 sidebar" style="margin-top:20px;"><br>
                		<?php echo get_sidebar(); ?>
                	</div>
                    
                </div>
        
        </div>
    </section>