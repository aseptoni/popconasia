<?php
/*
Template Name: Activities Sidebar
*/
get_header();

?>

<br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                <h2 class="section-heading" style="color: white;">ACTIVITIES</h2>
                </div>
                <div class="col-md-3">
                <div class="text-right"><br><select class="form-control">
                    <option>Orderby</option>
                    <option>Date</option>
                    <option>Name</option>
                </select>
                </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
            <?php

                $perpage = 8;
				$args = array( 'post_type' => 'activities', 'posts_per_page' => $perpage, 'paged'=>$paged
                 );

				 $the_query = query_posts(
						array(
							'post_type'=>'activities',
							'posts_per_page'=>$perpage,
							'paged'=>$paged
						)
					);
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
               // $wp_query = new WP_Query($args);
              ?>

              <div class="row">
              	<div class="col-lg-8">
                	  <div class="row text-center"><br><br>
            <?php
                while($wp_query->have_posts()) : the_post();
                ?>
                <div class="col-md-4 col-lg-4 col-xs-6 konten"><a href="<?php the_permalink(); ?>">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                    <h4 class="service-heading"><?php echo get_the_title(); ?></h4>
                    <p class="text-muted"><?php the_excerpt(); ?></p>
                </div>
                <?php endwhile; ?>






                <div class="col-md-12">

                <?php wp_pagenavi(); ?>

                </div>
            </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 sidebar"><br>
                	 <?php echo get_sidebar(); ?>
                </div>
              </div>

          <!-- container -->
        </div>
    </section>

<?php
get_footer();
?>