<div style="background:#FAFAFB; margin-top:0px; padding:10px 15px 15px 15px;">


<?php
  $idna = get_the_id();
   $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

   if ("http://popconasia.com/blog" !== $url) {
?>
<h4>LATEST NEWS</h4>

  <div class="row">

  	<?php
		 $args = array(
		 	'post_type' => 'post',
			'posts_per_page' => 6,
      'post__not_in' => array($idna),
			'orderby'=>'post_date',
			'orderby'=>'DESC'
         );

         $wp_query = new WP_Query($args);
		  while($wp_query->have_posts()) : the_post();
	?>
  	<div class="col-md-12 col-sm-6 col-xs-12" style="margin-bottom:10px;">
    	<div class="row news-sidebar">

          <div class="col-md-4 col-xs-5 col-sm-6 img" >
            <div style="background-image: url('<?php echo the_post_thumbnail_url('full'); ?>'); background-size: cover; height: 100px;  background-position: center; width:100%;">
            </div>
          </div>

      <div class="col-md-8 col-xs-7 col-sm-6 caption"><a href="<?=get_permalink();?>"><?php echo the_title(); ?></a>
      <br>
      <?php the_date('d F Y', '<small>', '</small>'); ?>
      </div>
  </div>
    </div>
     <?php endwhile; ?>
  </div>
  <br><br>
<?php
  }
?>

<h4>FEATURED</h4>
  <div style="list-style:none;">
	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
    <?php endif; ?>
    </div>
</div>