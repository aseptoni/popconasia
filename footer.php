<?php
$copy = get_theme_mod("copyright");
$footer_desc = get_theme_mod("footer_desc");
 ?>

    <!-- footer Section -->
    <br><br>
    <section class="bawah">
        <div class="container">
            <div class="row"><br><br><br><br>
                <div class="col-sm-8 no-padding">
                    <div class="col-md-4 col-lg-4 col-sm-6" style="padding-bottom: 20px;">
                        <h4 class="service-heading">Popcon Asia</h4>
                        <p class="text-muted"><?=$footer_desc;?></p>
                        <div style="margin-top:40px;">
                        
                        	<?php
								$fbLogo = get_option('popcon_general_facebook_logo');
								$fbLogo = wp_get_attachment_url($fbLogo[0]);
								$fbLink = get_option('popcon_general_facebook_link');
								
								$twLogo = get_option('popcon_general_twitter_logo');
								$twLogo = wp_get_attachment_url($twLogo[0]);
								$twLink = get_option('popcon_general_twitter_link');
								
								$igLogo = get_option('popcon_general_instagram_logo');
								$igLogo = wp_get_attachment_url($igLogo[0]);
								$igLink = get_option('popcon_general_instagram_link');
								
								$ytLogo = get_option('popcon_general_youtube_logo');
								$ytLogo = wp_get_attachment_url($ytLogo[0]);
								$ytLink = get_option('popcon_general_youtube_link');
							
							?>
                            
                        	<a href="<?=$fbLink?>" target="_blank">
                            	<img src="<?=$fbLogo?>"
                                width="25"/>
                            </a>
                            <a href="<?=$twLink?>" target="_blank">
                            	<img src="<?=$twLogo?>"
                                width="25"/>
                            </a>
                            <a href="<?=$igLink?>" target="_blank">
                            	<img src="<?=$igLogo?>"
                                width="25"/>
                            </a>
                            <a href="<?=$ytLink?>" target="_blank">
                            	<img src="<?=$ytLogo?>"
                                width="25"/>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6" style="padding-bottom: 20px;">
                        <h4 class="service-heading">Exhibitor Info</h4>
                        <p class="text-muted">
                            <?php
                            $menu = wp_get_nav_menu_object("footer1");
                            $menu_items = wp_get_nav_menu_items($menu->term_id);

                            foreach($menu_items as $row){
                            ?>
                            <a href='<?php echo $row->url; ?>' style='color:white;'>
                                <?php
                                echo "<h5>".$row->title."</h5></a>";
                                }
                                ?>
                        </p>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12" style="padding-bottom: 20px;">
                        <h4 class="service-heading">Visitor Info</h4>
                        <p class="text-muted">
                            <?php
                            $menu = wp_get_nav_menu_object("footer2");
                            $menu_items = wp_get_nav_menu_items($menu->term_id);

                            foreach($menu_items as $row){
                            ?>
                            <a href='<?php echo $row->url; ?>' style='color:white;'>
                                <?php
                                echo "<h5>".$row->title."</h5></a>";
                                }
                                ?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 no-padding">
                    <div class="col-xs-12">
                        <div class="fb-page" data-href="https://www.facebook.com/PopconAsia/" data-tabs="timeline" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/PopconAsia/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/PopconAsia/">Popcon ASIA</a></blockquote></div><br>
                    </div>
                </div>


            </div>
        </div><br>
        <div class="foot">
            <div class="text-center" style="background-color: #E2595D; padding: 10px; color: white; margin-top: 5px;">
                <?= $copy; ?>
            </div>
        </div>
    </section>

	<!-- jQuery -->
<!--    <script src="--><?php //bloginfo( 'template_directory' ); ?><!--/bootstrap/js/jquery.js"></script>-->

    <!-- Bootstrap Core JavaScript -->
<!--    <script src="--><?php //bloginfo( 'template_directory' ); ?><!--/bootstrap/js/bootstrap.min.js"></script>-->
<!--      <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
    <script type="text/javascript" src="<?=get_template_directory_uri()?>/slick/slick.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('.autoplay').slick({
  slidesToShow: 4,
  slidesToScroll: 4
});

    });

	function getPathFromUrl(url) {
  return url.split("?")[0];
}

	//Exhibitor
	$('.filter-select').change(function(e) {
		var url      	= getPathFromUrl(window.location.href);
		var thisval 		= $(this).val();
		var orderVal 	= $('.order-select').val();
		window.location = url+'?filter='+thisval+'&order='+orderVal;

    });

	$('.order-select').change(function(e) {
		var thisval 		= $(this).val();
		var filterVal	= $('.filter-select').val();
		var url      	= getPathFromUrl(window.location.href);
		window.location = url+'?filter='+filterVal+'&order='+thisval;
	});

	//Schedule
	$('.filter-select-schedule').change(function(e) {
		var url      	= getPathFromUrl(window.location.href);
		var thisval 		= $(this).val();
		window.location = url+'?filter='+thisval;

    });

	//Gif - nogif
	$('.img-nogif').hover(
    function() {
        $(this).addClass("hide");
		$(this).next('.img-gif').removeClass("hide");
    }
	);

	$( ".img-gif" ).mouseout(function() {
			$(this).addClass("hide");
			$(this).prev('.img-nogif').removeClass("hide");
	});



  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89931599-1', 'auto');
  ga('send', 'pageview');

</script>
<?php
 wp_footer();
?>
</body>
</html>