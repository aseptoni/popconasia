<?php
/*
Template Name: Exhibitor
*/
get_header();

if ( get_query_var('paged') ) {

$paged = get_query_var('paged');

} elseif ( get_query_var('page') ) {

$paged = get_query_var('page');

} else {

   $paged = 1;

}
$filter="";
if(isset($_GET['filter'])){
$filter = $_GET['filter'];
}
$order="";
if(isset($_GET['order'])){
$order = $_GET['order'];
}
?>

    <br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-xs-12">    
                <h2 class="section-heading" style="color: white; text-transform: uppercase; padding: 5px 0px;"><?php echo the_title(); ?></h2>
                </div>
                <div class="col-md-3 col-xs-12" style="padding-bottom: 25px;">
                 <div class="text-right"><br>
                	<div class="row">
                    	<div class="col-lg-6 col-xs-6">
                        	<?php
						$categories = get_terms( 'exhibitor_categories');
						// var_dump($categories);
							?>
                            <select class="form-control filter-select">
                    <option value="">No filter</option>
					<?php foreach($categories as $rowCat):
						$select="";
						if($filter==$rowCat->slug){
							$select="selected='selected'";
						}
					?>
                     <option value="<?=$rowCat->slug;?>" <?=$select?>><?=$rowCat->name;?></option>
                     <?php endforeach; ?>
                </select>
                        </div>
                        <div class="col-lg-6 col-xs-6">
                        	<select class="form-control order-select">
                            	
                                <?php
									$arr = array(
										'newest' => 'Newest',
										'oldest' => 'Oldest',
										'smallest-booth-number' => 'Smallest booth number',
										'largest-booth-number' => 'Largest booth number',
										'az' => 'A-Z',
										'za' => 'Z-A'

									);
								?>
                                
                                <?php foreach($arr as $index=>$value):
									$select="";
									if($order==$index){
										$select="selected='selectd'";
									}
								 ?>
                    				<option <?=$select;?> value="<?=$index?>"><?=$value?></option>
                                <?php endforeach; ?>
                               
                            </select>
                        </div>
                        
                    </div>
                
               
                	
                
                </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
            <?php
			
				
			
                $perpage = 12;
                
				
				if($order=="newest"){
					$orderArg = array(
						'orderby'  	=> 'date',
						'order' 		=> 'DESC'
					);
				}elseif($order=="oldest"){
					$orderArg = array(
						'orderby'  	=> 'date',
						'order' 		=> 'ASC'
					);
				}elseif($order=="smallest-booth-number"){
					$orderArg = array(
						'orderby'  	=> 'meta_value',
						'meta_key' 	=> 'booth_number',
						'order' 		=> 'ASC'
					);
				}elseif($order=="largest-booth-number"){
					$orderArg = array(
						'orderby'  	=> 'meta_value',
						'meta_key' 	=> 'booth_number',
						'order' 		=> 'DESC'
					);
				}elseif($order=="az"){
					$orderArg = array(
						'orderby'  	=> 'title',
						'order' 		=> 'ASC'
					);
				}elseif($order=="za"){
					$orderArg = array(
						'orderby'  	=> 'title',
						'order' 		=> 'DESC'
					);
				}else{
					$orderArg = array(
						'orderby'  	=> 'date',
						'order' 		=> 'DESC'
					);
				}
				
				if($filter){
					$args = array(
						'post_type' => 'exhibitor',
						'posts_per_page' => $perpage,
						'paged'=> $paged,
						'tax_query' => array(
							array(
								'taxonomy' => 'exhibitor_categories',
								'field' => 'slug',
								'terms' => $filter
							)
						)
                 	);
					
					$args = array_merge($args,$orderArg);
				}else{
					$args = array(
					'post_type' => 'exhibitor',
					'posts_per_page' => $perpage,
					'paged'=> $paged
                 	);
					$args = array_merge($args,$orderArg);
				}
				
				//echo "<pre>";
				//var_dump($args); exit;
				
				 /*
				 	
				 */
                $wp_query = new WP_Query($args);
              ?>
            <div class="row text-center"><br><br>
            <?php
                while($wp_query->have_posts()) : the_post();
					
					$pod = pods( 'exhibitor', get_the_id() );
					$booth = $pod->field('booth_number');
                ?>   
                <div class="col-md-2 col-xs-12" style=""><a href="<?php the_permalink(); ?>">
                <?php
                  if (has_post_thumbnail()) {
                   ?>
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="">
                    <?php  }else{ ?>
                    <img src="http://dev.popconasia.com/wp-content/uploads/2017/01/Untitled-2-01.png" class="img-responsive" alt="">
                    <?php } ?>
                    </a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;">
                    <h4 class="service-heading"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted" style="margin-top:-10px;"><?=$booth;?></p>
                </div>
                <?php endwhile; ?>
                <div class="col-md-12">
                <br><br>
                <?php wp_pagenavi(); ?>
                
                </div>
            </div>
        </div>
    </section>

    

<?php
get_footer();
?>