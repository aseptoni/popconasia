<?php
/*
Template Name: Modcon
*/
get_header();



?>

 <br><br>


<div class="modcon">
    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <div class="row">
            
                <div class="col-md-6 col-xs-12">    
                <h2 class="section-heading section-heading-title">
					<?php echo the_title(); ?></h2>
                </div>
                
                <div class="col-md-6 col-xs-12 modcon-submenu">
                 <div>
                 <?php
				 	$submenuId = get_option( 'modcon_submenu_ID');
				 	$modconMenu = wp_get_nav_menu_items($submenuId);
					$countMenu = count($modconMenu);
					$i=0;
					foreach($modconMenu as $row):
					$i++;
				 ?>
                 
                 <a href="<?=$row->url?>" > 
                 
                 <?php if($i!=1): ?>
                 &nbsp; &nbsp;
                 <? endif; ?>
				 
				 <?=ucfirst($row->title);?>
                 </a>
                 
				 <? if($countMenu!=$i){ ?>  &nbsp; &nbsp;  / <?php } ?>
                 <?php endforeach; ?>
                 
                 
                </div>
                
             </div>
        </div>
        </section>
        
    
    <!-- Hero -->
    <!-- @todo : media query no padding on mobile -->
    <section class="modcon-hero">
    	<div class="container">
        	<div class="row">
                <div class="col-lg-12" >
                    <div class="modcon-hero-bg">
                        <div class="row">
                            <div class="col-lg-6 modcon-hero-left">
                                <h2><?=get_option( 'modcon_hero_title'); ?></h2>
                                <p><?=get_option( 'modcon_hero_description'); ?></p>
                                <a href="<?=get_option( 'modcon_hero_link'); ?>" 
                                target="<?=get_option('modcon_hero_link_target')?>">
                                    <button class="btn btn-more">
                                    <?=get_option( 'modcon_hero_link_label'); ?>
                                    </button>
                                </a>
                            </div>
                               <div class="col-lg-6">
                               <?php
                                    $heroMedia = get_option( 'modcon_hero_media');
                                    $heroMedia = wp_get_attachment_url( $heroMedia[0] );
                                    $heroMediaType = get_option( 'modcon_hero_media_type');
                                    if($heroMediaType=="Image"){
                               ?>
                                <img src="<?=$heroMedia;?>" width="100%"/>				
                                <?php }elseif($heroMediaType=="Video"){ ?>
									<div class="embed-responsive embed-responsive-16by9">
                                    	<?php
										$heroMediayoutube = get_option( 'modcon_hero_media_youtube');
										?>
                                      <iframe class="embed-responsive-item" src="<?=$heroMediayoutube?>"></iframe>
                                    </div>
								<?php }?>
        </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>
    
    <!-- Aussie Banget -->
    <?php
	$aussieBangetBg = get_option( 'modcon_aussie_banget_background');
	$aussieBangetBg = wp_get_attachment_url( $aussieBangetBg[0] );
	?>
    <section class="modcon-aussiebgt" style="background:#e2595d;">
    	<div class="container">
    	<div class="row">
        	<div class="col-lg-4 hide">
            	&nbsp;
            </div>
            <div class="col-lg-8 col-lg-offset-2 text-center col-xs-12 modcon-aussiebgt-content">
            	<h2><?=get_option( 'modcon_aussie_banget_title'); ?></h2>
                <p><?=get_option( 'modcon_aussie_banget_desciption'); ?></p>
            </div>
        </div>
        </div>
    </section>
    
    <!-- Competition Process -->
    <section>
    	<div class="container text-center modcon-process">
        	<div class="row modcon-process-container">
            	<div class="col-lg-12">
                	<h2><?=get_option( 'modcon_process_title'); ?></h2>
                    <div class="modcon-divider"></div>
                </div>
            </div>
            
            <div class="row modcon-process-content">
            	<div class="col-lg-10 col-lg-offset-1">
                	<?=get_option( 'modcon_process_overview_content'); ?>
                </div>
            </div>
            
        </div>
    </section>
    
     <!-- Judges -->
    <section class="modcon-judges">
    	<div class="container">
        	<div class="modcon-judges-container">
        	<div class="row">
            	<div class="col-lg-8 col-lg-offset-2">
                	<div class="row text-center">
                    	<div class="col-lg-12 modcon-judges-heading">
                        	<h2 style="" > <?=get_option( 'modcon_judges_title'); ?></h2>
                    		<div class="modcon-divider"></div>
                        </div>
                        <?=get_option( 'modcon_judges_content'); ?>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
    
    <!-- Call to action -->
    <section class="modcon-call-action">
    	<div class="container">
        	<div class="row">
            	<?=get_option( 'modcon_call_to_action_content'); ?>
            </div>
        </div>
    </section>
    
    <!-- Socmed -->
    <section class="modcon-socmed">
    	<div class="container text-center">
        	<div class="row">
            	<?=get_option( 'modcon_social_media_content'); ?>
            </div>
        </div>
    </section>
   
   </div> 
    
<?php
get_footer();
?>