<?php
get_header();

$pod = pods( 'exhibitor', get_the_id() );
$idna = get_the_id();

$booth = $pod->field('booth_number');
$link = $pod->field('link');
?>


<br><br>


    <!-- Title Section -->
        <section class="judulatas">
        <div class="container">
            <br><a href="http://dev.popconasia.com/exhibitor-temp/"><span class="label label-default" >Exhibitor</span></a>
            <h2 class="section-heading" style="color: white; margin-top:0px; text-transform: uppercase;">
                <?php echo get_the_title(); ?>
            </h2>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container">
            <div class="row"><br>
              <div class="col-md-8 col-lg-8 col-sm-12">
                <div class="row">
                  <div class="col-md-6">
                  <?php
                    if (has_post_thumbnail()) {
                   ?>
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" width="300px">
                    <?php  }else{ ?>
                    <img src="http://dev.popconasia.com/wp-content/uploads/2017/01/Untitled-2-01.png" width="300px">
                    <?php } ?>
                  </div>
                  <div class="col-md-6">
                    <p>  <?php the_post(); the_content(); ?> </p>
                    <p>Booth Number : <?php echo $booth; ?></p>
                      <a href="<?php echo $link; ?>"><button class="btn btn-popcon">LINK</button></a>
                  </div>
                  </div>

<?php
$activities = $pod->field( 'activities' );
if ( ! empty( $activities ) ) {
?>
 <!-- activities --><br><br>
                  <div class="panel panel-default">
                    <div class="panel-heading"><h5>ACTIVITIES</h5></div>
                    <div class="panel-body">
                      <div class="row text-center"><br><br>
                       <?php
  foreach ( $activities as $rel ) {
    $id = $rel[ 'ID' ];
    ?>
                          <div class="col-md-3 col-lg-3 col-sm-6"><a href="<?php echo get_the_permalink($id); ?>">
                              <img src="<?php echo get_the_post_thumbnail_url($id); ?>" class="img-responsive" alt="" ></a>
                              <h4 class="service-heading"><?php echo get_the_title($id); ?></h4>
                              <p class="text-muted"><?php get_the_excerpt($id); ?></p>
                          </div>
<?php
    }
?>
                      </div>
                    </div>
                  </div>
              <!-- end speaker -->
<?php
}
?>

<?php
$speaker = $pod->field( 'speaker' );
if (! empty( $speaker )) {
?>
<!-- speaker -->
                  <div class="panel panel-default">
                    <div class="panel-heading"><h5>SPEAKER</h5></div>
                    <div class="panel-body">
                      <div class="row text-center"><br><br>
                    <?php
  foreach ( $speaker as $rel ) {
    $id = $rel[ 'ID' ];
    ?>

                          <div class="col-md-3 col-lg-3 col-sm-6"><a href="<?php echo get_the_permalink($id); ?>">
                              <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt=""></a>
                              <h4 class="service-heading"><?php echo get_the_title($id); ?></h4>
                              <p class="text-muted">Web</p>
                          </div>
<?php
    }

?>
                      </div>
                    </div>
                  </div>
<!-- end speaker -->
<?php
}
?>
   <!-- More exhibitor -->
            <?php
            $perpage = 6;
            $the_query = query_posts(
            array(
            'post__not_in' => array($idna),
            'post_type'=>'exhibitor',
            'posts_per_page'=>$perpage,
            'paged'=>$paged,
            'orderby'=>'rand'
            )
            );
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            // $wp_query = new WP_Query($args);
            ?>
            <div class="panel panel-default">
            <div class="panel-heading"><h5>MORE EXHIBITOR</h5></div>
            <div class="panel-body">
            <div class="row text-center">
            <?php
            while($wp_query->have_posts()) : the_post();
            $pod = pods( 'exhibitor', get_the_id() );
          $booth = $pod->field('booth_number');
                ?>
                <div class="col-md-2 col-xs-6"><a href="<?php the_permalink(); ?>">
                <?php
                    if (has_post_thumbnail()) {
                   ?>
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="">
                <?php }else{ ?>
                    <img src="http://dev.popconasia.com/wp-content/uploads/2017/01/Untitled-2-01.png" class="img-responsive" alt="">
                    <?php } ?>
                    </a>
                    <a href="<?php the_permalink(); ?>" style="color:black; text-decoration: none;">
                    <h4 class="service-heading"><?php echo get_the_title(); ?></h4></a>
                    <p class="text-muted" style="margin-top:-10px;"><?=$booth;?></p>
                </div>

            <?php endwhile; ?>
            </div>
            </div>
            </div>
      <!-- End exhibitor -->
      </div>
               <div class="col-md-4 col-lg-4 col-sm-12 sidebar">
                  <?php
                    get_sidebar();
                  ?>
              </div>

            </div>
        </div>
    </section>

<?php
    get_footer();
?>