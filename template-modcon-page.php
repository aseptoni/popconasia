<?php
/*
Template Name: Modcon Page
*/
get_header();
?>

<div style="background-color:#F3F3F3;">

<br><br>


    <!-- Title Section -->
    <section class="judulatas">
        <div class="container">
            <h2 class="section-heading" style="color: white; padding: 5px 0px;">
			<small style="color:#FFFFFF; font-size:14px;">Modcon 2017</small><br/><?php echo the_title(); ?>
            </h2>
        </div>
    </section>

    <!-- Body Section -->
    <section>
        <div class="container body-single body-modcon">
            <div class="row"><br>
              <div class="col-md-8">
              
              <div style="background-color:#fff; padding:10px 30px; padding-bottom:30px;">
                  
                  <br>
                  <p style="text-align: justify;"><?php
        // echo '<p>Posted on '.get_the_date('M d, Y').'</p>';
        the_post();
    the_content();
        ?>
                  </p>
                 
                  <!-- <button class="btn btn-popcon">SHARE</button> -->
                  <?php comments_template(); ?> 
                  </div>
              </div>
              <div class="col-md-4">
              
              	<div>
                	
                	<?php
                                    $heroMedia = get_option( 'modcon_hero_media');
                                    $heroMedia = wp_get_attachment_url( $heroMedia[0] );
                                    $heroMediaType = get_option( 'modcon_hero_media_type');
                                    if($heroMediaType=="Image"){
                               ?>
                                <img src="<?=$heroMedia;?>" width="100%"/>				
                                <?php } ?>
                </div>
                
                  <div style="background-color:#fff;">
                  		<ul class="list-group list-modcon">
                         <li class="list-group-item"><a href="<?=get_site_url()."/";?><?=get_option( 'modcon_overview_slug'); ?>">Overview</a></li>
						<?php 
						$submenuId = get_option( 'modcon_submenu_ID');
						$modconMenu = wp_get_nav_menu_items($submenuId);
						foreach($modconMenu as $row):
						 ?>
                         
  <li class="list-group-item"> <a href="<?=$row->url?>"> <?=ucfirst($row->title);?></a></li>
  			<?php endforeach; ?>
</ul>
                        
                        
                  </div>
              </div>
            </div>
        </div>
    </section>
    <br><br>

    </div>
    
     <!-- Call to action -->
    <section class="modcon-call-action">
    	<div class="container">
        	<div class="row">
            	<?=get_option( 'modcon_call_to_action_content'); ?>
            </div>
        </div>
    </section>
    
        <!-- Socmed -->
    <section class="modcon-socmed">
    	<div class="container text-center">
        	<div class="row">
            	<?=get_option( 'modcon_social_media_content'); ?>
            </div>
        </div>
        
        
    </section>


<?php get_footer(); ?>